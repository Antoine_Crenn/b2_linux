#!/bin/bash
# Antoine Crenn
# 15/10/2020
# Permet de mettre en place le reverse proxy avec nginx et de faire sa sauvegarde
# vers la VM serveur

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.12  mariadb
192.168.2.14  serveur" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet epel-release pour ajouter des dêpots de paquets.

sudo yum install -y epel-release

# Permet d'installer le paquet gninx pour pouvoir monter un serveur web.

sudo yum install -y nginx

# Permet d'installer le paquet nfs-utils qui permet à un ordianteur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Rempli le fichier de configuration du serveur nginx pour qu'il agisse comme
# un reverse proxy en direction du service Gitea.

echo "# Indique le nombre de processus lancé par le service.
worker_processes 1;
# Permet d'indiquer où enregistrer les log.
error_log nginx_error.log;
# Permet d'indiquer dans quel fichier est enregistrer le PID du processus.
pid /run/nginx.pid;
# Indique le nombre de connection maximales en simultané.
events {
    worker_connections 1024;
}

http {
# Indique sur quel port écoute le serveur http et indique le nom du serveur.
    server {
      listen 80;
      server_name gitea;

    location / {
      # Permet de redirigé la connection sur la VM vers le service Gitea
        proxy_pass http://192.168.2.11:3000;
    }
  }
}"  | sudo tee /etc/nginx/nginx.conf

# Permet d'ouvrir le port 80/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=80/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/nginx de la VM serveur.

sudo mount 192.168.2.14:/opt/nginx /media/nfs

# Permet de créer le dossier backup dans le dossier opt, c'est là que
# l'on va enregistrer le script de sauvegarde pour la configuration du reverse proxy.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit backup
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown backup:backup /opt/backup

# Le script de backup qui permet de sauvegarder la configuration du reverse proxy.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Reverse Proxy

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"reverse_proxy_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";
chemin_reverse_proxy=\"/etc/nginx/nginx.conf\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder la configuration du reverse proxy.

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_reverse_proxy} --ignore-failed-read | tee /dev/null;

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de backup pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * backup bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab

# Permet de lancer le service nginx.

sudo systemctl start nginx
