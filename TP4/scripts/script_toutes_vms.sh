#!/bin/bash
# Antoine Crenn
# 18/10/2020
# Permet de spécifier l'adresse du DNS, de créer l'utilisateur backup,
# et de configurer Netdata.

# La variable qui sera utilisée dans le script.

url='https://discord.com/api/webhooks/761268162905767946/a9ubvnEhLd7wQvmmScZ1kU-wbmsXwijidKzSs8op6ZQIeH5kXdqwwlFkMwJm24IRUpD6';

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.

echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf

# Créer l'utilisateur backup qui n'a pas de home.

sudo useradd backup -M

# Permet au système de trouvé les fichiers de configuration netdata.

/etc/netdata/edit-config health_alarm_notify.conf;

# Permet d'indiquer que l'on veut recevoir des alarmes grâce à Discord.

sudo sed -i "s/DEFAULT_RECIPIENT_DISCORD=\"\"/DEFAULT_RECIPIENT_DISCORD=\"alarms\"/" /etc/netdata/health_alarm_notify.conf

# Permet d'ajouter l'URL du WEBHOOK discord pour que l'on reçoit les notications pour le site sur un salon Discord dédié.

sudo sed -i "s/DISCORD_WEBHOOK_URL=\"\"/DISCORD_WEBHOOK_URL=\"${url}\"/" /etc/netdata/health_alarm_notify.conf

# Permet d'ouvrir le port 19999/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=19999/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload;
