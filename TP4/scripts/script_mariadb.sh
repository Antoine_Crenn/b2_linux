#!/bin/bash
# Antoine Crenn
# 14/10/2020
# Permet de mettre en place la base de données MariaDB et de configurer Netdata.

# La variable qui sera utilisée dans le script.

mot_passe="mot_de_passe"

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.13  nginx
192.168.2.14  serveur" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet mariadb-server qui permet de mettre en place
# un serveur de base de données SQL.

sudo yum install -y mariadb-server

# Permet d'installer le paquet nfs-utils qui permet à un ordianteur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet que le service mariadb soit lancé au boot de la machine.

sudo systemctl enable mariadb.service

# Permet de lancer le service mariadb.

sudo systemctl start mariadb.service

# Permet de créer un utilisateur pour MariaDB et lui associe son mot de passe,
# créer la base de données et donne tous les droits à l'utilisateur que l'on a
# crée sur la base de données que l'on a crée.

echo -e "CREATE USER 'gitea'@'192.168.2.11' IDENTIFIED BY \"${mot_passe}\";\n CREATE DATABASE depot CHARACTER SET 'utf8mb4'     COLLATE 'utf8mb4_unicode_ci';\n GRANT ALL PRIVILEGES ON depot.* TO 'gitea'@'192.168.2.11';\n FLUSH PRIVILEGES;\n exit    \n " | mysql -u root

# Permet d'ouvrir le port 3006/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=3306/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/mariadb de la VM serveur.

sudo mount 192.168.2.14:/opt/mariadb /media/nfs

# Permet de relancer le service mariadb pour qu'il prend en compte les modifications.

sudo systemctl restart mariadb.service
