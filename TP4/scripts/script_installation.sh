#!/bin/bash
# Antoine Crenn
# 13/10/2020
# Fait les mises à jour, installe vim et netdata
# désactive le SELINUX et lance le firewall au boot.

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.

echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf

# Permet de faire les mises à jour des paquets présents sur la VM.

sudo yum update -y;

# Permet d'installer le paquet vim qui est un éditeur de texte très puissant.

sudo yum install -y vim;

# Permet d'installer Netdata sans devoir intervenir lors de l'installation.

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

# Permet de mettre SELinux en permissive jusqu'au prochain redémarrage.

sudo setenforce 0;

# Permet de modifier SELINUX=enforcing par SELINUX=permissive dans le fichier /etc/selinux/config
# pour que SELinux soit en permissive de façon permanente.

sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;

# Permet d'activer le firewall pour qu'il se lance au boot.

sudo systemctl enable firewalld;
