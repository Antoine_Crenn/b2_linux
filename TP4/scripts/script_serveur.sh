#!/bin/bash
# Antoine Crenn
# 16/10/2020
# Permet de mettre en place le serveur NFS et d'associer un dossier à chaque VM.

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.12  mariadb
192.168.2.13  nginx" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet nfs-utils qui permet à un ordianteur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet d'ouvrir le port tcp associé au service nfs au niveau du firewall
# de façon permanent.

sudo firewall-cmd --add-service=nfs --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet d'écrire le domaine des VMs dans le fichier de configuration idmapd.conf
# pour le service nfs.

sudo sed -i "s/#Domain = local.domain.edu/Domain = tp4.b2/" /etc/idmapd.conf;

# Permet de créer les dossiers gitea, mariadb et nginx dans le dossier opt.

sudo mkdir /opt/gitea /opt/mariadb /opt/nginx

# Permet d'indiquer que l'on veut partager les dossiers gitea, mariadb et nginx
# qui sont dans le dossier opt sur le réseau 192.168.2.0/24
# et de les mettrent en lecture et écriture.

echo "/opt/gitea  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)
/opt/mariadb  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)
/opt/nginx  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)" | sudo tee /etc/exports

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit backup
# le propriétaire et le groupe propriétaire sur les dossiers mariadb et nginx
# qui se trouvent dans le dossier opt.

sudo chown backup:backup /opt/mariadb /opt/nginx

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit nfsnobody
# le propriétaire et le groupe propriétaire sur le dossier gitea qui se trouvent
# dans le dossier opt.

sudo chown nfsnobody:nfsnobody /opt/gitea

# Permet que les services rpc et nfs soient lancés au boot de la machine.

sudo systemctl enable rpcbind nfs-server --now
