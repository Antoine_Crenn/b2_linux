#!/bin/bash
# Antoine Crenn
# 13/10/2020
# Permet de mettre en place le service Gitea, de faire sa sauvegarde vers la VM serveur
# et de vérifier l'empreinte du binaire Gitea.

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.12  mariadb
192.168.2.13  nginx
192.168.2.14  serveur" | sudo tee -a /etc/hosts

# Permet d'installer le paquet wget qui permet de télécharger depuis Internet.

sudo yum install wget -y

# Permet d'installer le paquet git qui permet la gestion de versions décentralisées.

sudo yum install git -y

# Permet d'installer le paquet nfs-utils qui permet à un ordianteur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet d'installer le paquet Gitea depuis Internet.

wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64

# Permet de créer le dossier bonus dans le dossier opt, c'est là que l'on va enregistrer
# les deux empreintes et le script de vérification.

mkdir /opt/bonus

# Permet de récupérer l'empreinte sha256 fournie par Gitea.

wget -O /opt/bonus/gitea.sha256 https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-arm64.sha256

# Permet de calculer l'empreinte depuis le binaire.

sha256sum gitea | sudo tee /opt/bonus/empreite_binaire

# Le script qui permet de comparer les deux empreintes, celle du binaire et
# celle que l'on a récupérer.

echo "#!/bin/bash
# Antoine Crenn
# 19/10/2020
# Script Différence Empreinte

# Permet de vérifier s'il y a une différence entre les deux empreintes.

diff /opt/bonus/empreite_binaire /opt/bonus/gitea.sha256" | sudo tee /opt/bonus/empreinte.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier empreinte.sh.

sudo chmod 700 /opt/bonus/empreinte.sh

# Permet de mettre les autorisations d'exécution sur Gitea.

chmod +x gitea

# Créer l'utilisateur git qui à comme home /home/git et qui à /bib/bash comme shell.

sudo adduser --system --shell /bin/bash --home /home/git git

# Permet de créer les dossiers custom, data et log et de créer les dossiers
# parents s'ils n'existent pas.

sudo mkdir -p /var/lib/gitea/{custom,data,log}

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit git
# le propriétaire et le groupe propriétaire et que ça se fasse sur tous les éléments
# qui sont dans le dossier gitea.

sudo chown -R git:git /var/lib/gitea/

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'exécution
# au propriétaire pour tous les éléments qui sont dans le dossier gitea et de mettre
# les autorisations de lecture et d'exécution au groupe pour tous les éléments
# qui sont dans le dossier gitea.

sudo chmod -R 750 /var/lib/gitea/

# Permet de créer le dossier gitea dans le dossier etc.

sudo mkdir /etc/gitea

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit root
# le propriétaire et que ce soit git le groupe propriétaire sur le dossier /etc/gitea.

sudo chown root:git /etc/gitea

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'exécution
# au propriétaire et au groupe sur le dossier /etc/gitea.

sudo chmod 770 /etc/gitea

# Permet de remplir le fichier app.ini du service Gitea pour qu'il utilise
# la base de données que l'on va créer sur la VM mariadb et pour qu'il passe
# par le reverse proxy que l'on va configurer sur la VM nginx.

echo "APP_NAME = Gitea Depot
RUN_USER = root
RUN_MODE = prod

[oauth2]
JWT_SECRET = w3s7rSw71mI1Ecpuk8Mm5WeeCheQvSAJiHVqe1jQ3G0

[security]
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MDM1NjQ0MTZ9.Zl6Oc6WWcErfm6BZaHa82sOzfrkj7McdevJ9OzuGbAA
INSTALL_LOCK   = true
SECRET_KEY     = RrxsgPycYzslGB43FLMUnLy2LlejftIJEy7gDuHCw8IHPSU0UGOdmwEbPsdMwdkj

[database]
DB_TYPE  = mysql
HOST     = 192.168.2.12:3306
NAME     = depot
USER     = gitea
PASSWD   = mot_de_passe
SCHEMA   =
SSL_MODE = disable
CHARSET  = utf8
PATH     = /usr/local/bin/data/gitea.db

[repository]
ROOT = /root/gitea-repositories

[server]
SSH_DOMAIN       = 192.168.2.13
DOMAIN           = tp4_b2
HTTP_PORT        = 3000
ROOT_URL         = http://192.168.2.13:80/
DISABLE_SSH      = false
SSH_PORT         = 22
LFS_START_SERVER = true
LFS_CONTENT_PATH = /usr/local/bin/data/lfs
LFS_JWT_SECRET   = m5oMw88giXGETRULOPcOVtd5vDwnko2wgq6Mo_VwIho
OFFLINE_MODE     = false

[mailer]
ENABLED = false

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /usr/local/bin/log" | sudo tee /etc/gitea/app.ini

# Permet d'ouvrir le port 3000/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=3000/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Rempli le fichier qui va permettre de créer le service gitea.

echo "[Unit]
# Permet de rédiger la description du service gitea.
Description=Service Git qui se nomme Gitea
# Permet d'indiquer que le service Gitea se lancera après ces services.
After=syslog.target
After=network.target
# Indique que le service requis le service MariaDB.
#Requires=mariadb.service

[Service]
# Indique le temps de mise en veille avant de redémarrer le service.
RestartSec=2s
# Indique le type du service lancé.
Type=simple
# Permet de spécifier quel utilisateur lance le service.
User=git
# Permet de spécifier quel groupe lance le service.
Group=git
# Indique le répertoire de travail du processus.
WorkingDirectory=/var/lib/gitea/
# Lance le service Gitea.
ExecStart=/usr/bin/sudo /usr/local/bin/gitea web --config /etc/gitea/app.ini
# Permet de relancer le service Gitea s'il a échoué.
Restart=always
# Permet de mettre l'utilisateur qui lance le service en variable d'environnement.
Environment=\"USER=git\"
# Permet d'indiquer le home de l'utilisateur qui lance le service.
HOME=/home/git
# Indique le répertoire de travail de Gitea.
GITEA_WORK_DIR=/var/lib/gitea

[Install]
# Permet d'indiquer que le service a la possibilité d'être lancé au démarrage de la machine.
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/gitea.service

# Permet d'ajouter les utilisateurs git et nfsnobody aux utilisateurs qui peuvent
# faire des commandes sudo.

echo "git     ALL=(ALL)  NOPASSWD: ALL
nfsnobody     ALL=(ALL)  NOPASSWD: ALL" | sudo tee -a /etc/sudoers

# Permet de copier gitea vers /usr/local/bin/gitea pour que l'on puisse lancer
# l'exécutable de gitea.

sudo cp gitea /usr/local/bin/gitea

# Permet d'enlever l'autorisation d'écriture au groupe sur le dossier /etc/gitea.

sudo chmod 750 /etc/gitea

# Permet de mettre seulement les autorisations de lecture et d'écriture au propriétaire
# sur le fichier /etc/gitea/app.ini et de mettre l'autorisation de lecture au groupe
# sur le fichier /etc/gitea/app.ini.

sudo chmod 640 /etc/gitea/app.ini

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/gitea de la VM serveur.

sudo mount 192.168.2.14:/opt/gitea /media/nfs

# Permet de créer le dossier backup dans le dossier opt,
# c'est là que l'on va enregistrer le script de sauvegarde pour Gitea.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit nfsnobody
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown nfsnobody:nfsnobody /opt/backup

# Le script de backup qui permet de sauvegarder Gitea.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Gitea

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"gitea_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder Gitea.

sudo ./gitea dump -c /etc/gitea/app.ini;

# La commande qui permet de trouver le nom de l'archive Gitea.

fichier=\$(sudo ls /home/vagrant/ | grep .zip)

# La commande qui permet de copier l'archive vers le dossier lié au serveur NFS.

sudo cp \${fichier} \${chemin_sauvegarde}\${nom_sauvegarde};

# La commande qui permet de supprimer l'archive Gitea après sa copie.

sudo rm -rf \${fichier};

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur nfsnobody et le groupe nfsnobody qui soient propriétaire.

sudo chown nfsnobody:nfsnobody /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de nfsnobody pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * nfsnobody bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab

# Permet d'ajouter l'utilisateur nfsnobody au groupe git.

sudo usermod -G git nfsnobody

# Indique au système qu'il faut relire les fichiers ce qui permet qu'il trouve
# le service que l'on a ajouté.

sudo systemctl daemon-reload

# Permet que le service gitea soit lancé au boot de la machine.

sudo systemctl enable gitea.service

# Permet de lancer le service gitea.

sudo systemctl start gitea.service
