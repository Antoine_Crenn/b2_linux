# B2 Linux 2020 - TP4

## TP4: Déploiement multi-noeud

### 0. Prérequis

On va utilisé une box centos/7, où on va faire les mises à jour des paquets, installer vim, désactiver SELinux et lancer le firewall et comme on va avoir besoin de Netdata sur toutes les VMs on va aussi installer le paquet Netdata. Et après on va la repackager pour pouvoir l'utiliser de base. Pour ce faire on a écrit un Vagrantfile et un script d'installation. 

On veut afficher le contenu du Vagrantfile qui permet de créer la VM que l'on va repackagé et d'exécuter le script d'installation.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.define "test" do |test_debut|
    test_debut.vm.hostname = "test"
    test_debut.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    test_debut.vm.provision :shell, path: "script_installation.sh"
    test_debut.vm.provider "virtualbox" do |test|
      test.name = "test"
      test.memory = "1024"
    end
  end
end
```

[Vagrantfile du prérequis.](./vagrantfile/Vagrantfile_prerequis)

Et maintanant, on veut afficher le contenu du script d'installation qui permet d'installer les paquets et de faire la configuration de base pour la VM que l'on va repackagé.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_installation.sh
#!/bin/bash
# Antoine Crenn
# 13/10/2020
# Fait les mises à jour, installe vim et netdata
# désactive le SELINUX et lance le firewall au boot.

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.

echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf

# Permet de faire les mises à jour des paquets présents sur la VM.

sudo yum update -y;

# Permet d'installer le paquet vim qui est un éditeur de texte très puissant.

sudo yum install -y vim;

# Permet d'installer Netdata sans devoir intervenir lors de l'installation.

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

# Permet de mettre SELinux en permissive jusqu'au prochain redémarrage.

sudo setenforce 0;

# Permet de modifier SELINUX=enforcing par SELINUX=permissive dans le fichier /etc/selinux/config
# pour que SELinux soit en permissive de façon permanente.

sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;

# Permet d'activer le firewall pour qu'il se lance au boot.

sudo systemctl enable firewalld;
```

[Script qui permet les installations.](./scripts/script_installation.sh)

La commande que l'on va faire pour packager la VM que l'on vient de créer et configuré grâce au Vagrantfile et grâce au script d'installation.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> vagrant package --output centos7_tp4.box
==> test: Attempting graceful shutdown of VM...
==> test: Clearing any previously set forwarded ports...
==> test: Exporting VM...
==> test: Compressing package to: C:/Users/Crenn Antoine/Desktop/Cours/B2/Linux/vagrant/centos7_tp4.box
```

Le résultat de la commande nous montre que la VM a bien été packagé.

La commande qui permet de créer la box centos7_tp4 à partir de la VM que l'on vient de packager.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> vagrant box add centos7_tp4 centos7_tp4.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos7_tp4' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/Crenn%20Antoine/Desktop/Cours/B2/Linux/vagrant/centos7_tp4.box
    box:
==> box: Successfully added box 'centos7_tp4' (v0) for 'virtualbox'!
```

Le résultat de la commande nous montre que la box centos7_tp4 a bien était créer à partir de la VM packagé.

### I. Consignes générales

On va créer une VM pour chaque service que l'on veut mettre en place, sur chaque des VMs on va associé le nom des autres VMs à leurs adresses IP avec le fichier /etc/hosts et le pare-feu sera configurés pour laisser passer le minimum et elles seront monitorées.

Dans le tableau suivant, on peut voir la liste des hosts, qui permet de voir leurs noms, leurs adresses IP et leurs rôles.

| Name           | IP           | Rôle        |
| -------------- | ------------ | ----------- |
| gitea.tp4.b2   | 192.168.2.11 | Gitea       |
| mariadb.tp4.b2 | 192.168.2.12 | MariaDB     |
| nginx.tp4.b2   | 192.168.2.13 | Nginx       |
| serveur.tp4.b2 | 192.168.2.14 | Serveur NFS |

L'interface Web pour acceder à Gitea est 192.168.2.13.

Les interfaces de Monitoring des VMs sont 192.168.2.11:19999 pour la VM gitea, 192.168.2.12:19999 pour la VM mariadb, 192.168.2.13:19999 pour la VM nginx et 192.168.2.14:19999 pour la VM serveur.

On va faire que pour la partie de sauvegarde, la sauvegarde s'exécute tous les jours.

Pour le monitoring, on veut que les alertes remontent dans un discord et on peut le modifier dans le script_toutes_vms.sh voir partie 6 grand 2.

### II. Présentation de la stack

Pour la mise en place de ces différents services, on va créer une VM par service ce qui fait que l'on va créer 4 VMs. On va donc créer un Vagrantfile qui créer 4 VMs qui leurs donnent un hostname, un nom dans Vagrant et une adresse IP, ce Vagrantfile permettra aussi de déposer une clé publique sur chaque VM et il va permettre d'exécuter les différents scripts que l'on va utiliser pour mettre en place les services. Les scripts que l'on va utiliser pour la mise en place des différents services sont au nombre de 5 car un pour chaque VM et un en commum entre toutes les VMs. L'ordre de lancement des machines est la VM serveur, puis la VM gitea, puis la VM mariadb et enfin la VM nginx.

Maintenant on veut afficher le contenu du Vagrantfile qui permet de créer les VMs et d'exécuter les scripts.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos7_tp4"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.provision :shell, path: "script_toutes_vms.sh"
  config.vm.define "serveur" do |serveur_debut|
    serveur_debut.vm.hostname = "serveur.tp4.b2"
    serveur_debut.vm.network "private_network", ip: "192.168.2.14", netmask:"255.255.255.0"
    serveur_debut.vm.provision :shell, path: "script_serveur.sh"
    serveur_debut.vm.provider "virtualbox" do |serveur|
      serveur.name = "serveur"
      serveur.memory = "1024"
    end
  end
  config.vm.define "gitea" do |gitea_debut|
    gitea_debut.vm.hostname = "gitea.tp4.b2"
    gitea_debut.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    gitea_debut.vm.provision :shell, path: "script_gitea.sh"
    gitea_debut.vm.provider "virtualbox" do |gitea|
      gitea.name = "gitea"
      gitea.memory = "1024"
    end
  end
  config.vm.define "mariadb" do |mariadb_debut|
    mariadb_debut.vm.hostname = "mariadb.tp4.b2"
    mariadb_debut.vm.network "private_network", ip: "192.168.2.12", netmask:"255.255.255.0"
    mariadb_debut.vm.provision :shell, path: "script_mariadb.sh"
    mariadb_debut.vm.provider "virtualbox" do |mariadb|
      mariadb.name = "mariadb"
      mariadb.memory = "1024"
    end
  end
  config.vm.define "nginx" do |nginx_debut|
    nginx_debut.vm.hostname = "nginx.tp4.b2"
    nginx_debut.vm.network "private_network", ip: "192.168.2.13", netmask:"255.255.255.0"
    nginx_debut.vm.provision :shell, path: "script_nginx.sh"
    nginx_debut.vm.provider "virtualbox" do |nginx|
      nginx.name = "nginx"
      nginx.memory = "1024"
    end
  end
end
```

[Vagrantfile de la mise en place des services.](./vagrantfile/Vagrantfile_services)

#### 1. Gitea

Le premier service que l'on va mettre en place est le service Gitea pour ce faire on a créer un script qui s'appelle script_gitea.sh qui installe Gitea, qui met en place le service Gitea, qui remplit le fichier /etc/hosts, créer le lien avec la VM serveur.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_gitea.sh
#!/bin/bash
# Antoine Crenn
# 13/10/2020
# Permet de mettre en place le service Gitea, de faire sa sauvegarde vers la VM serveur
# et de vérifier l'empreinte du binaire Gitea.

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.12  mariadb
192.168.2.13  nginx
192.168.2.14  serveur" | sudo tee -a /etc/hosts

# Permet d'installer le paquet wget qui permet de télécharger depuis Internet.

sudo yum install wget -y

# Permet d'installer le paquet git qui permet la gestion de versions décentralisées.

sudo yum install git -y

# Permet d'installer le paquet nfs-utils qui permet à un ordinateur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet d'installer le paquet Gitea depuis Internet.

wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64

# Permet de créer le dossier bonus dans le dossier opt, c'est là que l'on va enregistrer
# les deux empreintes et le script de vérification.

mkdir /opt/bonus

# Permet de récupérer l'empreinte sha256 fournie par Gitea.

wget -O /opt/bonus/gitea.sha256 https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-arm64.sha256

# Permet de calculer l'empreinte depuis le binaire.

sha256sum gitea | sudo tee /opt/bonus/empreite_binaire

# Le script qui permet de comparer les deux empreintes, celle du binaire et
# celle que l'on a récuperé.

echo "#!/bin/bash
# Antoine Crenn
# 19/10/2020
# Script Différence Empreinte

# Permet de vérifier s'il y a une différence entre les deux empreintes.

diff /opt/bonus/empreite_binaire /opt/bonus/gitea.sha256" | sudo tee /opt/bonus/empreinte.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier empreinte.sh.

sudo chmod 700 /opt/bonus/empreinte.sh

# Permet de mettre les autorisations d'exécution sur Gitea.

chmod +x gitea

# Créer l'utilisateur git qui à comme home /home/git et qui à /bib/bash comme shell.

sudo adduser --system --shell /bin/bash --home /home/git git

# Permet de créer les dossiers custom, data et log et de créer les dossiers
# parents s'ils n'existent pas.

sudo mkdir -p /var/lib/gitea/{custom,data,log}

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit git
# le propriétaire et le groupe propriétaire et que ça se fasse sur tous les éléments
# qui sont dans le dossier gitea.

sudo chown -R git:git /var/lib/gitea/

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'exécution
# au propriétaire pour tous les éléments qui sont dans le dossier gitea et de mettre
# les autorisations de lecture et d'exécution au groupe pour tous les éléments
# qui sont dans le dossier gitea.

sudo chmod -R 750 /var/lib/gitea/

# Permet de créer le dossier gitea dans le dossier etc.

sudo mkdir /etc/gitea

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit root
# le propriétaire et que ce soit git le groupe propriétaire sur le dossier /etc/gitea.

sudo chown root:git /etc/gitea

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'exécution
# au propriétaire et au groupe sur le dossier /etc/gitea.

sudo chmod 770 /etc/gitea

# Permet de remplir le fichier app.ini du service Gitea pour qu'il utilise
# la base de données que l'on va créer sur la VM mariadb et pour qu'il passe
# par le reverse proxy que l'on va configurer sur la VM nginx.

echo "APP_NAME = Gitea Depot
RUN_USER = root
RUN_MODE = prod

[oauth2]
JWT_SECRET = w3s7rSw71mI1Ecpuk8Mm5WeeCheQvSAJiHVqe1jQ3G0

[security]
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MDM1NjQ0MTZ9.Zl6Oc6WWcErfm6BZaHa82sOzfrkj7McdevJ9OzuGbAA
INSTALL_LOCK   = true
SECRET_KEY     = RrxsgPycYzslGB43FLMUnLy2LlejftIJEy7gDuHCw8IHPSU0UGOdmwEbPsdMwdkj

[database]
DB_TYPE  = mysql
HOST     = 192.168.2.12:3306
NAME     = depot
USER     = gitea
PASSWD   = mot_de_passe
SCHEMA   =
SSL_MODE = disable
CHARSET  = utf8
PATH     = /usr/local/bin/data/gitea.db

[repository]
ROOT = /root/gitea-repositories

[server]
SSH_DOMAIN       = 192.168.2.13
DOMAIN           = gitea
HTTP_PORT        = 3000
ROOT_URL         = http://192.168.2.13:80/
DISABLE_SSH      = false
SSH_PORT         = 22
LFS_START_SERVER = true
LFS_CONTENT_PATH = /usr/local/bin/data/lfs
LFS_JWT_SECRET   = m5oMw88giXGETRULOPcOVtd5vDwnko2wgq6Mo_VwIho
OFFLINE_MODE     = false

[mailer]
ENABLED = false

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /usr/local/bin/log" | sudo tee /etc/gitea/app.ini

# Permet d'ouvrir le port 3000/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=3000/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Rempli le fichier qui va permettre de créer le service gitea.

echo "[Unit]
# Permet de rédiger la description du service gitea.
Description=Service Git qui se nomme Gitea
# Permet d'indiquer que le service Gitea se lancera après ces services.
After=syslog.target
After=network.target
# Indique que le service requis le service MariaDB.
#Requires=mariadb.service

[Service]
# Indique le temps de mise en veille avant de redémarrer le service.
RestartSec=2s
# Indique le type du service lancé.
Type=simple
# Permet de spécifier quel utilisateur lance le service.
User=git
# Permet de spécifier quel groupe lance le service.
Group=git
# Indique le répertoire de travail du processus.
WorkingDirectory=/var/lib/gitea/
# Lance le service Gitea.
ExecStart=/usr/bin/sudo /usr/local/bin/gitea web --config /etc/gitea/app.ini
# Permet de relancer le service Gitea s'il a échoué.
Restart=always
# Permet de mettre l'utilisateur qui lance le service en variable d'environnement.
Environment=\"USER=git\"
# Permet d'indiquer le home de l'utilisateur qui lance le service.
HOME=/home/git
# Indique le répertoire de travail de Gitea.
GITEA_WORK_DIR=/var/lib/gitea

[Install]
# Permet d'indiquer que le service a la possibilité d'être lancé au démarrage de la machine.
WantedBy=multi-user.target" | sudo tee /etc/systemd/system/gitea.service

# Permet d'ajouter les utilisateurs git et nfsnobody aux utilisateurs qui peuvent
# faire des commandes sudo.

echo "git     ALL=(ALL)  NOPASSWD: ALL
nfsnobody     ALL=(ALL)  NOPASSWD: ALL" | sudo tee -a /etc/sudoers

# Permet de copier gitea vers /usr/local/bin/gitea pour que l'on puisse lancer
# l'exécutable de gitea.

sudo cp gitea /usr/local/bin/gitea

# Permet d'enlever l'autorisation d'écriture au groupe sur le dossier /etc/gitea.

sudo chmod 750 /etc/gitea

# Permet de mettre seulement les autorisations de lecture et d'écriture au propriétaire
# sur le fichier /etc/gitea/app.ini et de mettre l'autorisation de lecture au groupe
# sur le fichier /etc/gitea/app.ini.

sudo chmod 640 /etc/gitea/app.ini

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/gitea de la VM serveur.

sudo mount 192.168.2.14:/opt/gitea /media/nfs

# Permet de créer le dossier backup dans le dossier opt,
# c'est là que l'on va enregistrer le script de sauvegarde pour Gitea.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit nfsnobody
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown nfsnobody:nfsnobody /opt/backup

# Le script de backup qui permet de sauvegarder Gitea.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Gitea

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"gitea_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder Gitea.

sudo ./gitea dump -c /etc/gitea/app.ini;

# La commande qui permet de trouver le nom de l'archive Gitea.

fichier=\$(sudo ls /home/vagrant/ | grep .zip)

# La commande qui permet de copier l'archive vers le dossier lié au serveur NFS.

sudo cp \${fichier} \${chemin_sauvegarde}\${nom_sauvegarde};

# La commande qui permet de supprimer l'archive Gitea après sa copie.

sudo rm -rf \${fichier};

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur nfsnobody et le groupe nfsnobody qui soient propriétaire.

sudo chown nfsnobody:nfsnobody /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de nfsnobody pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * nfsnobody bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab

# Permet d'ajouter l'utilisateur nfsnobody au groupe git.

sudo usermod -G git nfsnobody

# Indique au système qu'il faut relire les fichiers ce qui permet qu'il trouve
# le service que l'on a ajouté.

sudo systemctl daemon-reload

# Permet que le service gitea soit lancé au boot de la machine.

sudo systemctl enable gitea.service

# Permet de lancer le service gitea.

sudo systemctl start gitea.service
```

[Script qui met en place le service Gitea.](./scripts/script_gitea.sh)

#### 2. MariaDB

Le deuxième service que l'on va mettre en place est la base de données MariaDB qui sera utilisé par le service Gitea, on a donc créer un script qui se nomme script_mariadb.sh qui permet d'installer MariaDB, de le configurer, de créer une base de données, de remplir le fichier /etc/hosts et de créer le lien avec la VM serveur.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_mariadb.sh
#!/bin/bash
# Antoine Crenn
# 14/10/2020
# Permet de mettre en place la base de données MariaDB et de configurer Netdata.

# La variable qui sera utilisée dans le script.

mot_passe="mot_de_passe"

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.13  nginx
192.168.2.14  serveur" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet mariadb-server qui permet de mettre en place
# un serveur de base de données SQL.

sudo yum install -y mariadb-server

# Permet d'installer le paquet nfs-utils qui permet à un ordinateur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet que le service mariadb soit lancé au boot de la machine.

sudo systemctl enable mariadb.service

# Permet de lancer le service mariadb.

sudo systemctl start mariadb.service

# Permet de créer un utilisateur pour MariaDB et lui associe son mot de passe,
# créer la base de données et donne tous les droits à l'utilisateur que l'on a
# crée sur la base de données que l'on a crée.

echo -e "CREATE USER 'gitea'@'192.168.2.11' IDENTIFIED BY \"${mot_passe}\";\n CREATE DATABASE depot CHARACTER SET 'utf8mb4'     COLLATE 'utf8mb4_unicode_ci';\n GRANT ALL PRIVILEGES ON depot.* TO 'gitea'@'192.168.2.11';\n FLUSH PRIVILEGES;\n exit    \n " | mysql -u root

# Permet d'ouvrir le port 3006/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=3306/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/mariadb de la VM serveur.

sudo mount 192.168.2.14:/opt/mariadb /media/nfs

# Permet de relancer le service mariadb pour qu'il prend en compte les modifications.

sudo systemctl restart mariadb.service
```

[Script qui met en place la base de données MariaDB.](./scripts/script_mariadb.sh)

#### 3. NGINX

Le troisième service que l'on va mettre en place est le reverse-proxy qui redirigera vers le service Gitea, on va créer un script qui se nomme script_nginx.sh qui permet d'installer Nginx, de le configurer pour qu'il redirige vers le service Gitea, de remplir le fichier /etc/hosts et de créer le lien avec la VM serveur.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_nginx.sh
#!/bin/bash
# Antoine Crenn
# 15/10/2020
# Permet de mettre en place le reverse proxy avec nginx et de faire sa sauvegarde
# vers la VM serveur

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.12  mariadb
192.168.2.14  serveur" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet epel-release pour ajouter des dêpots de paquets.

sudo yum install -y epel-release

# Permet d'installer le paquet gninx pour pouvoir monter un serveur web.

sudo yum install -y nginx

# Permet d'installer le paquet nfs-utils qui permet à un ordinateur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Rempli le fichier de configuration du serveur nginx pour qu'il agisse comme
# un reverse proxy en direction du service Gitea.

echo "# Indique le nombre de processus lancé par le service.
worker_processes 1;
# Permet d'indiquer où enregistrer les log.
error_log nginx_error.log;
# Permet d'indiquer dans quel fichier est enregistrer le PID du processus.
pid /run/nginx.pid;
# Indique le nombre de connection maximales en simultané.
events {
    worker_connections 1024;
}

http {
# Indique sur quel port écoute le serveur http et indique le nom du serveur.
    server {
      listen 80;
      server_name gitea;

    location / {
      # Permet de redirigé la connection sur la VM vers le service Gitea
        proxy_pass http://192.168.2.11:3000;
    }
  }
}"  | sudo tee /etc/nginx/nginx.conf

# Permet d'ouvrir le port 80/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=80/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet de créer le dossier nfs et de créer le dossier media s'il n'existe pas.

mkdir -p /media/nfs

# Permet de monter le point de montage à partir du dossier /media/nfs avec
# le dossier /opt/nginx de la VM serveur.

sudo mount 192.168.2.14:/opt/nginx /media/nfs

# Permet de créer le dossier backup dans le dossier opt, c'est là que
# l'on va enregistrer le script de sauvegarde pour la configuration du reverse proxy.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit backup
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown backup:backup /opt/backup

# Le script de backup qui permet de sauvegarder la configuration du reverse proxy.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Reverse Proxy

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"reverse_proxy_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";
chemin_reverse_proxy=\"/etc/nginx/nginx.conf\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder la configuration du reverse proxy.

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_reverse_proxy} --ignore-failed-read | tee /dev/null;

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de backup pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * backup bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab

# Permet de lancer le service nginx.

sudo systemctl start nginx
```

[Script qui met en place le reverse proxy Nginx.](./scripts/script_nginx.sh)

#### 4. Serveur NFS

Le dernier service que l'on va mettre en place est le serveur NFS pour ce faire on a créer un script qui s'appelle script_serveur.sh qui installe NFS, le configure pour notre réseau, qui créer les 3 dossiers qui seront associés aux 3 autres VMs et de remplir le fichier /etc/hosts.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_serveur.sh
#!/bin/bash
# Antoine Crenn
# 16/10/2020
# Permet de mettre en place le serveur NFS et d'associer un dossier à chaque VM.

# On associé l'adresse IP de chaque VM à son nom grâce au fichier /etc/hosts.

echo "192.168.2.11  gitea
192.168.2.12  mariadb
192.168.2.13  nginx" | sudo tee -a /etc/hosts;

# Permet d'installer le paquet nfs-utils qui permet à un ordinateur
# à accéder à des fichiers grâce au réseau.

sudo yum install nfs-utils -y

# Permet d'ouvrir le port tcp associé au service nfs au niveau du firewall
# de façon permanent.

sudo firewall-cmd --add-service=nfs --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload

# Permet d'écrire le domaine des VMs dans le fichier de configuration idmapd.conf
# pour le service nfs.

sudo sed -i "s/#Domain = local.domain.edu/Domain = tp4.b2/" /etc/idmapd.conf;

# Permet de créer les dossiers gitea, mariadb et nginx dans le dossier opt.

sudo mkdir /opt/gitea /opt/mariadb /opt/nginx

# Permet d'indiquer que l'on veut partager les dossiers gitea, mariadb et nginx
# qui sont dans le dossier opt sur le réseau 192.168.2.0/24
# et de les mettrent en lecture et écriture.

echo "/opt/gitea  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)
/opt/mariadb  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)
/opt/nginx  192.168.2.0/24(rw,async,no_subtree_check) \
*tp4.b2(rw,async,no_subtree_check)" | sudo tee /etc/exports

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit backup
# le propriétaire et le groupe propriétaire sur les dossiers mariadb et nginx
# qui se trouvent dans le dossier opt.

sudo chown backup:backup /opt/mariadb /opt/nginx

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit nfsnobody
# le propriétaire et le groupe propriétaire sur le dossier gitea qui se trouvent
# dans le dossier opt.

sudo chown nfsnobody:nfsnobody /opt/gitea

# Permet que les services rpc et nfs soient lancés au boot de la machine.

sudo systemctl enable rpcbind nfs-server --now
```

[Script qui met en place le serveur NFS.](./scripts/script_serveur.sh)

#### 5. Sauvegarde

La sauvegarde pour Gitea se fait avec le script qui s'exécute sur la VM gitea grâce aux lignes suivantes.

```bash
# Permet de créer le dossier backup dans le dossier opt,
# c'est là que l'on va enregistrer le script de sauvegarde pour Gitea.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit nfsnobody
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown nfsnobody:nfsnobody /opt/backup

# Le script de backup qui permet de sauvegarder Gitea.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Gitea

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"gitea_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder Gitea.

sudo ./gitea dump -c /etc/gitea/app.ini;

# La commande qui permet de trouver le nom de l'archive Gitea.

fichier=\$(sudo ls /home/vagrant/ | grep .zip)

# La commande qui permet de copier l'archive vers le dossier lié au serveur NFS.

sudo cp \${fichier} \${chemin_sauvegarde}\${nom_sauvegarde};

# La commande qui permet de supprimer l'archive Gitea après sa copie.

sudo rm -rf \${fichier};

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur nfsnobody et le groupe nfsnobody qui soient propriétaire.

sudo chown nfsnobody:nfsnobody /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de nfsnobody pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * nfsnobody bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab

# Permet d'ajouter l'utilisateur nfsnobody au groupe git.

sudo usermod -G git nfsnobody
```

[Script qui met en place le service Gitea.](./scripts/script_gitea.sh)

La commande qui permet de lancer le script de sauvegarde pour Gitea est la suivante.

```bash
sudo -u nfsnobody bash /opt/backup/sauvegarde.sh
```

La sauvegarde pour le reverse proxy se fait avec le script qui s'exécute sur la VM nginx grâce aux lignes suivantes.

```bash
# Permet de créer le dossier backup dans le dossier opt, c'est là que
# l'on va enregistrer le script de sauvegarde pour la configuration du reverse proxy.

mkdir /opt/backup

# Permet de changer le propriétaire et le goupe propriétaire pour que ce soit backup
# le propriétaire et le groupe propriétaire sur le dossier /opt/backup.

sudo chown backup:backup /opt/backup

# Le script de backup qui permet de sauvegarder la configuration du reverse proxy.

echo "#!/bin/bash
# Antoine Crenn
# 17/10/2020
# Script Sauvegarde Reverse Proxy

# Les variables qui seront utilisé dans le script.

heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
nom_sauvegarde=\"reverse_proxy_\${jour}_\${heure}\";
chemin_fermer=\"/opt/backup/bloque\";
chemin_sauvegarde=\"/media/nfs/\";
chemin_reverse_proxy=\"/etc/nginx/nginx.conf\";

# Test qui vérifie s'il y a déjà une sauvegarde en cours grâce à la présence d'un fichier.

if test -f \${chemin_fermer};
then
        exit 22;
fi

# Créer le fichier qui bloque la sauvegarde.

touch \${chemin_fermer};

# La commande pour sauvegarder la configuration du reverse proxy.

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_reverse_proxy} --ignore-failed-read | tee /dev/null;

# Test qui vérifie si la sauvegarde c'est bien passé.

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
fi

# Supprime le fichier qui bloque la sauvegarde s'il y a déjà une sauvegarde en cours.

rm -rf \${chemin_fermer};" | sudo tee /opt/backup/sauvegarde.sh

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /opt/backup/sauvegarde.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier sauvegarde.sh.

sudo chmod 700 /opt/backup/sauvegarde.sh

# Permet de faire que le script de backup pour Gitea se lance toutes les jours.

echo "  0  0  *  *  * backup bash /opt/backup/sauvegarde.sh" | sudo tee -a /etc/crontab
```

La commande qui permet de lancer le script de sauvegarde pour le reverse proxy est la suivante.

```bash
sudo -u backup bash /opt/backup/sauvegarde.sh
```

[Script qui met en place le reverse proxy Nginx.](./scripts/script_nginx.sh)

#### 6. Monitoring

On veut aussi faire du monitoring sur les 4 VMs pour ce faire on va configurer le service Netdata que l'on a installé dans la box que l'on a repackagé dans les prérequis et cette configuration on le fait dans le script qui s'exécutent sur toutes les VMs. Ce script permet d'installer Netdata et de le configurer. Et pour spécifier l'URL du discord que l'on veut utiliser, il faut juste remplacer l'URL présente dans la variable url du script_toutes_vms.sh par l'URL du discord que vous voulez.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_toutes_vms.sh
#!/bin/bash
# Antoine Crenn
# 18/10/2020
# Permet de spécifier l'adresse du DNS, de créer l'utilisateur backup 
# et de configurer Netdata.

# La variable qui sera utilisée dans le script.

url='https://discord.com/api/webhooks/761268162905767946/a9ubvnEhLd7wQvmmScZ1kU-wbmsXwijidKzSs8op6ZQIeH5kXdqwwlFkMwJm24IRUpD6';

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.

echo "nameserver 1.1.1.1" | sudo tee /etc/resolv.conf

# Créer l'utilisateur backup qui n'a pas de home.

sudo useradd backup -M

# Permet au système de trouvé les fichiers de configuration netdata.

/etc/netdata/edit-config health_alarm_notify.conf;

# Permet d'indiquer que l'on veut recevoir des alarmes grâce à Discord.

sudo sed -i "s/DEFAULT_RECIPIENT_DISCORD=\"\"/DEFAULT_RECIPIENT_DISCORD=\"alarms\"/" /etc/netdata/health_alarm_notify.conf

# Permet d'ajouter l'URL du WEBHOOK discord pour que l'on reçoit les notications pour le site sur un salon Discord dédié.

sudo sed -i "s/DISCORD_WEBHOOK_URL=\"\"/DISCORD_WEBHOOK_URL=\"${url}\"/" /etc/netdata/health_alarm_notify.conf

# Permet d'ouvrir le port 19999/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=19999/tcp --permanent

# Permet de rédemarrer le firewall avec la modification de port.

sudo firewall-cmd --reload;
```

[Script qui met en place le service Monitoring.](./scripts/script_toutes_vms.sh)

#### 7. Bonus

##### Vérification du binaire Gitea

Nous voulons vérifier l'intégrité du binaire Gitea pour ce faire on récuperer l'empreinte sha256 et on calcule l'empreinte du binaire Gitea et grâce à un script on vérifier la différence entre les deux empreintes.

```bash
# Permet de créer le dossier bonus dans le dossier opt, c'est là que l'on va enregistrer
# les deux empreintes et le script de vérification.

mkdir /opt/bonus

# Permet de récupérer l'empreinte sha256 fournie par Gitea.

wget -O /opt/bonus/gitea.sha256 https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-arm64.sha256

# Permet de calculer l'empreinte depuis le binaire.

sha256sum gitea | sudo tee /opt/bonus/empreite_binaire

# Le script qui permet de comparer les deux empreintes, celle du binaire et
# celle que l'on a récupérer.

echo "#!/bin/bash
# Antoine Crenn
# 19/10/2020
# Script Différence Empreinte

# Permet de vérifier s'il y a une différence entre les deux empreintes.

diff /opt/bonus/empreite_binaire /opt/bonus/gitea.sha256" | sudo tee /opt/bonus/empreinte.sh

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'exécution
# à l'utilisateur sur le fichier empreinte.sh.

sudo chmod 700 /opt/bonus/empreinte.sh
```

[Script qui met en place le service Gitea.](./scripts/script_gitea.sh)

La commande pour lancer le script de vérification est la suivante.

```bash
bash /opt/bonus/empreinte.sh
```