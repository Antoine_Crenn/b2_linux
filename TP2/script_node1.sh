#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Script de configuration de la VM node1 que ça soit les sites,
# le backup et le netdata.

# Les différentes variables utilisées dans le script.
chemin_srv="/srv";
chemin_site1="${chemin_srv}/site1";
chemin_site2="${chemin_srv}/site2";
nom_fichier="index.html";
nom_cle="site.key";
nom_certificat="site.crt";
url="https:\/\/discord.com\/api\/webhooks\/761268162905767946\/a9ubvnEhLd7wQvmmScZ1kU-wbmsXwijidKzSs8op6ZQIeH5kXdqwwlFkMwJm24IRUpD6";

# La création des utilisateurs pour le script les deux ne peuvent pas se connecter,
# l'utilisateur web n'a pas de d'home et l'utilisateur backup à un home et appartient au groupe web.

sudo useradd web -M -s /sbin/nologin;

sudo useradd -m backup -G web -s /sbin/nologin;

# Créer les chemins des deux sites s'ils n'existent pas.

if test ! -d ${chemin_site1};
then
  sudo mkdir ${chemin_site1};
fi

if test ! -d ${chemin_site2};
then
  sudo mkdir ${chemin_site2};
fi

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'éxécution
# à l'utilisateur et au groupe sur les dossiers des deux sites.

sudo chmod 550 ${chemin_site1} ${chemin_site2};

# Associe l'adresse IP de la VM node2 au nom de la VM node2.

sudo echo "192.168.2.22  node2.tp2.b2" >> /etc/hosts;

# Remplir le fichier index.html du site 1.

sudo echo "<!DOCTYPE html>
<html lang=\"fr\">

<head>
  <meta charset=\"utf-8\">
  <title>Page Accueil</title>
  <meta name=\"description\" content=\"Page Accueil\" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>" > ${chemin_site1}/${nom_fichier};

# Remplir le fichier index.html du site 2.

sudo echo "<!DOCTYPE html>
<html lang=\"fr\">

<head>
  <meta charset=\"utf-8\">
  <title>Page Accueil</title>
  <meta name=\"description\" content=\"Page Accueil\" />
</head>

<body>
 <h1> Coucou vous etez sur le site 2</h1>
</body>" > ${chemin_site2}/${nom_fichier};

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur
# et au groupe sur les fichiers index.html des deux sites.

sudo chmod 440 ${chemin_site1}/${nom_fichier} ${chemin_site2}/${nom_fichier};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire des fichier index.html
# des deux sites pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_site1}/${nom_fichier} ${chemin_site2}/${nom_fichier};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire des dossiers
# des deux sites pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_site1}/ ${chemin_site2}/;

# Permet de créer la clé pour que les sites soient en https.

echo "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDfahs/nVslUswA
ro8KCyonBmqi73Dwby1gqVAQvycVNqBWgNjtWsy5bA/No+6WJp0o1Juajx/E2uZu
hshQZhhuOmUhVQNLZXA9IUqKdOmSNxl8W0zsECz5/6AnY2Ecglc02+1SLYJdx2nr
yjVo2r+jnEOkln+zI6Pn8JSVXlDqHO5/8F8+3NpJGbXrSQC15yNFMMAepmfwVypM
wU34ASNLFLkZnxhWy8Ta5e8+YNTY3Y7uUO9yqgGG5vu4V0BFkL8aoqpf5ky5+jje
K4VhHp1o3p1V/XTaQYhAzGuGdIpYcIYo02Lmvlra/6EyESlmTrKl83Zto5MXv8JR
LCQ4eFMxAgMBAAECggEANI5odJGW1HPiy7JEse8obl/DDCtZnSX6Pcf2rsnY7tOX
6mGLJmL60A/Ms+qBgM76n5VMOZK7vXplRNDuJIOv8ZZJHgs6qCznt2myeFnCbIQL
ucZclRfFDNaGnV1yhdtfmm62KGPOTDNSBobeA5maBWoIO5qv1btnnnNo8K5sSHYv
WgC3lsOevKAc7DbT0gSvqFfs5Jfqvuw8DKNtiq6kUjmy9Pp1HHyFbUvTJnfjduXz
ysRCVw1AxcZl2z6teNeDEYefTAO7ljkE2vUtx+yCgOI19i4DE0f81iIllzZIwAfk
tsDm6KZkjxULBl0n0JFA02DuY+/eDGhqRsn8ePaGcQKBgQDyBxZpIVUrduwFSf96
9N8gPPAfizPtG3NGT62H2W9XUtQOhmMErf/CeTEiWSnUkxW2IMWuPnh5eju4UKYi
rhl2QgncjLTDiwb/jImpLJDMDyMvZbkCHHXjrpQyZkeJ/7/bOjrdWdXMDivR4veF
ub6255VI/CZl8Da4G0XTlda1XQKBgQDsT+94wYBaDFTzqL88Lfw2Bp2P0BeLgXd+
SOeResY4BV7I6vxHWg/XpK/T+xmC+XT56wRewXWZM7ii8Jdy6pdRMp40Gfr2Mj6T
iNOt/eKlVzJB+n8cDuS7KytCWiVlmUonAsK8wMcP3P2emYEeeRXuBHH5fQIzHG95
Lb4Cm+gD5QKBgQCMQoc+Tc7p7XVCa2vfeeGE8UvHPAjPtETNzocmX2AjHyE9eawQ
njXGIi3TVhDj3ImMjtmA7TssSyYo3rQpDPQ8kHDem7x2Ri+8EVoF7InNeEhPw4fA
LXY0iIvh2VxEIS6ygcBzXwiDxQKoE/yWRtq7bOIa9eXa1acgcs24nPFdEQKBgQCL
YMI/mPGZKvjYFcllN0Iwo0o/Tl70u1G9PEbIuX87/37eRiqx8t6xM8M45jsv0u45
kXIsaTJxP95AzGKnNtI05AQHeuNYXl4mKmefkcUh/AH6hEKATBCAjH+hTQYNnJHt
3GT7a0B9jLxR1Pwg+w4CKsEGg8LOXpEpCd+uLkJ0OQKBgAl2T4dmu3WlUuT0Va0b
EChtSyf96dzYpqMk8wcskfTloUYCnQSLDvjoAzQMVUrf2RQ30VPji+V5fgHnE5i2
KcIW+2OVvcHFJCYq9Lm5qJMmNRa/F1Csm9r8jjTELy8Oj0sqmxmCFNBFyogECMh3
+oUknuTGb5GGxRhAYtToNOzm
-----END PRIVATE KEY-----" > /srv/site.key;

# Permet de créer le certificat ssl pour que les sites soient en https.

echo "-----BEGIN CERTIFICATE-----
MIIDATCCAemgAwIBAgIJALdc9e7u6JNuMA0GCSqGSIb3DQEBCwUAMBcxFTATBgNV
BAMMDG5vZGUxLnRwMi5iMjAeFw0yMDEwMDMxMjU5MTBaFw0yMTEwMDMxMjU5MTBa
MBcxFTATBgNVBAMMDG5vZGUxLnRwMi5iMjCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBAN9qGz+dWyVSzACujwoLKicGaqLvcPBvLWCpUBC/JxU2oFaA2O1a
zLlsD82j7pYmnSjUm5qPH8Ta5m6GyFBmGG46ZSFVA0tlcD0hSop06ZI3GXxbTOwQ
LPn/oCdjYRyCVzTb7VItgl3HaevKNWjav6OcQ6SWf7Mjo+fwlJVeUOoc7n/wXz7c
2kkZtetJALXnI0UwwB6mZ/BXKkzBTfgBI0sUuRmfGFbLxNrl7z5g1Njdju5Q73Kq
AYbm+7hXQEWQvxqiql/mTLn6ON4rhWEenWjenVX9dNpBiEDMa4Z0ilhwhijTYua+
Wtr/oTIRKWZOsqXzdm2jkxe/wlEsJDh4UzECAwEAAaNQME4wHQYDVR0OBBYEFKkr
s/LIJ5cMnG+O7oAgsnKqLugCMB8GA1UdIwQYMBaAFKkrs/LIJ5cMnG+O7oAgsnKq
LugCMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAKcQpusq3DpcVa1d
ZWpukdkL5NFEPsBUkMU6nLKUhmV0IV+GuwC6DVSzCPsohGy1FyuMRsKUst4rbRie
KbwRXdNYGJQCKxdqbqAaDeu/zIf1Srz+ylnq776+J+tjnSWFzTrdPhCeQX0cuoIJ
HhwE2U4gKZr8qhJiXpwVlF1WhNu0wFF1dkTyj5T21rYpuBF792Jj38tJaogTAmPD
zQEKRqRUaITYeM/puIvHwWrccHoxLiY058b20+XFLH4vYNSAiBkZULh4zkNMap+s
hZ3tznir1QUrF+0IEseLzRAuPe0H6L2tAaBXDa16QEzYE/brHgttC/b5zpk0zNhn
q/7b6k0=
-----END CERTIFICATE-----" > /srv/site.crt;

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur
# sur la clé du certificat.

sudo chmod 400 ${chemin_srv}/${nom_cle};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire de la clé
# pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_srv}/${nom_cle};

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur,
# au groupe et aux autres utilisateurs sur le certificat.

sudo chmod 444 ${chemin_srv}/${nom_certificat};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du certificat
# pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_srv}/${nom_certificat};

# Permet de définir la configuration pour les deux sites pour qu'ils soient en http et https
# et qu'ils soient relier à netdata.

sudo echo "worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;

events {
    worker_connections 1024;
}

http {

      upstream netdata {
        server 127.0.0.1:19999;
        keepalive 64;
    }

    server {
        listen 80;
        server_name node1.tp2.b2;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias ${chemin_site1}/${nom_fichier};
        }

        location /site2 {
            alias ${chemin_site2}/${nom_fichier};
        }

        location = /netdata {
            return 301 /netdata/;
        }

        location ~ /netdata/(?<ndpath>.*) {
            proxy_redirect off;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-Host \$host;
            proxy_set_header X-Forwarded-Server \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_pass_request_headers on;
            proxy_set_header Connection \"keep-alive\";
            proxy_store off;
            proxy_pass http://netdata/\$ndpath\$is_args\$args;
            gzip on;
            gzip_proxied any;
            gzip_types *;
        }
    }
    server {
        listen 443 ssl;

        server_name node1.tp2.b2;
        ssl_certificate ${chemin_srv}/${nom_certificat};
        ssl_certificate_key ${chemin_srv}/${nom_cle};

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias ${chemin_site1}/${nom_fichier};
        }

        location /site2 {
            alias ${chemin_site2}/${nom_fichier};
        }

        location = /netdata {
            return 301 /netdata/;
        }

        location ~ /netdata/(?<ndpath>.*) {
            proxy_redirect off;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-Host \$host;
            proxy_set_header X-Forwarded-Server \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_pass_request_headers on;
            proxy_set_header Connection \"keep-alive\";
            proxy_store off;
            proxy_pass http://netdata/\$ndpath\$is_args\$args;
            gzip on;
            gzip_proxied any;
            gzip_types *;
        }
    }
}" > /etc/nginx/nginx.conf;

# Permet d'ouvrir le port 80/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=80/tcp --permanent;

# Permet d'ouvrir le port 443/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=443/tcp --permanent;

# Permet d'ouvrir le port 19999/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=19999/tcp --permanent

# Permet de rédemarré le firewall avec les modifications de port.

sudo firewall-cmd --reload;

# Le script de backup qui permet de sauvegarder le site qui est passé en argument.

sudo echo -e "#!/bin/bash
# Antoine Crenn
# 01/10/2020
# Script Sauvegarde
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
nom_sauvegarde=\"\${nom_site}_\${jour}_\${heure}\";
chemin_fermer=\"/home/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/home/backup/\";

if test -z \${chemin_site};
then
        echo \"Vous avez oublié de rentrer l'argument.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${chemin_site};
then
        echo \"Le chemin du site \${chemin_site} n'existe pas.\" >&2;
        exit 1;
fi

if test -f \${chemin_fermer};
then
        exit 22;
fi

touch \${chemin_fermer};

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_site} --ignore-failed-read > /dev/null;

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
        exit 1;
fi

nombre_sauvegarde=\$(ls \${chemin_sauvegarde} | grep -c \${nom_site});

if test \${nombre_sauvegarde} -ge 9;
then
        supprimer=\$(ls \${chemin_sauvegarde} | grep \${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf \${chemin_sauvegarde}\${supprimer};
fi

rm -rf \${chemin_fermer};" > /home/backup/tp1_backup.sh;

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /home/backup/tp1_backup.sh;

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'éxécution
# à l'utilisateur sur le fichier tp1_backup.sh.

sudo chmod 700 /home/backup/tp1_backup.sh;

# Permet de faire que le script de backup se lance toutes les heures pour les deux sites.

sudo echo "  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site1

  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site2" >> /etc/crontab;

# Permet d'installer Netdata sans devoir intervenir lors de l'installation.

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

# Permet au système de trouvé les fichiers de configuration netdata.

/etc/netdata/edit-config health_alarm_notify.conf;

# Permet d'indiquer que l'on veut recevoir des alarmes grâce à Discord.

sudo sed -i "s/DEFAULT_RECIPIENT_DISCORD=\"\"/DEFAULT_RECIPIENT_DISCORD=\"alarms\"/" /etc/netdata/health_alarm_notify.conf;

# Permet d'ajouter l'URL du WEBHOOK discord pour que l'on reçoit les notications pour le site sur un salon Discord dédié.

sudo sed -i "s/DISCORD_WEBHOOK_URL=\"\"/DISCORD_WEBHOOK_URL=\"${url}\"/" /etc/netdata/health_alarm_notify.conf;

# Permet de lancer nginx avec les modifications de configuration.

sudo systemctl restart nginx;
