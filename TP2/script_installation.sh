#!/bin/bash
# Antoine Crenn
# 29/09/2020
# Fait les mises à jour, installe epel-release, nginx, vim
# désactive le SELINUX et lance le firewall au boot.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
sudo yum update -y;
sudo yum install epel-release -y;
sudo yum install nginx -y;
sudo yum install -y vim;
sudo setenforce 0;
sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;
sudo systemctl enable firewalld;
