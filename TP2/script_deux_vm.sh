#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Script pour la partie de configuration commune aux deux VMs.

# Permet de définir le serveur DNS que l'on veut utiliser.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf

# Les variables qui seront utilisées dans le script.
utilisateur="admin";
mot_de_passe="leagosselin"

# Permet de créer un utilisateur admin qui a un home.

sudo useradd $utilisateur -m;

# Permet d'ajouter un mot de passe à l'utilisateur admin.

echo -e "$mot_de_passe\n$mot_de_passe\n" | sudo passwd $utilisateur

# Permet d'ajouter l'utilisateur admin aux utilisateurs qui peuvent faire des commandes sudo.

sudo echo "$utilisateur   ALL=(ALL)       ALL" >> /etc/sudoers
