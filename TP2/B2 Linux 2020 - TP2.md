# B2 Linux 2020 - TP2

## TP2: Déploiement automatisé

### 0. Prérequis

#### Installez Vagrant

On va sur le site officiel de Vagrant pour télécharger Vagrant pour son OS. Vagrant est un outil qui permet de superviser un hyperviseur ici c'est VirtualBox. Vagrant permet de décrit une ou plusieurs VMs grâce au fichier Vagrantfile et permet d'automatisé la création de VM.

#### Init Vagrant

On va créer un dossier vagrant dans le dossier Linux pour ce faire on fait la commande mkdir Desktop/Cours/B2/Linux/vagrant.

```ruby
PS C:\Users\Crenn Antoine> mkdir Desktop/Cours/B2/Linux/vagrant
```

On va aller dans le dossier vagrant pour ce faire on fait la commande cd Desktop/Cours/B2/Linux/vagrant

```ruby
PS C:\Users\Crenn Antoine> cd Desktop/Cours/B2/Linux/vagrant
```

On modifie le fichier Vagrantfile pour qu'il ne vérifier pas s'il y a des mises à jour au démarrage et pour qu'il ne les fasse pas automatiquement les mises à jour.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
```

Le contenu du fichier Vagrantfile après la modification pour bloquer la vérification de mise à jour et ne pas les faire automatiquement.

On lance la VM avec la commande vagrant up.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: vagrant_default_1601368573275_88520
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
```

Maintenant on veut vérifier le status de la VM grâce à Vagrant pour ce faire on fait la commande vagrant status et on voit que la VM est bien en train de marcher.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.
```

Maintenant on se connecte à la VM grâce à Vagrant avec la commande vagrand ssh pour se connecter en ssh à la VM.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant ssh
```

Et maintenant on va détruire la VM avec la commande vagrant destroy et le -f pour ne pas avoir besoin de confirmer que l'on veut détruire la VM.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant destroy -f
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...
```

### I. Déploiement simple

On va créer un Vagrantfile pour qu'il utilise la box centos/7, qu'il crée une VM qui a un 1Go de RAM, une ip statique 192.168.2.11/24, spécifie un nom interne à Vagrant et spécifie un nom pour la VM.

La commande pour afficher le ficher Vagrantfile.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.network "private_network", ip: "192.168.2.11/24"
  config.vm.hostname = "test"
  config.vm.provider "virtualbox" do |v|
      v.name = "test"
      v.memory = 1024
      end
end
```

Les lignes que l'on a ajouté sont les suivantes la première c'est pour créer un interface Host-Only avec l'adresse ip 192.168.2.1/24 et donner l'adresse IP 192.168.2.11/24 à la VM, la deuxième donne à la VM le hostname test, le bloc suivant permet de donner le nom interne test à la VM pour Vagrant et de donner 1Go de RAM à la VM.

```ruby
config.vm.network "private_network", ip: "192.168.2.11/24"
config.vm.hostname = "test"
config.vm.provider "virtualbox" do |v|
    v.name = "test"
    v.memory = 1024
    end
```

On démarre la VM avec la commande vagrant up pour voir si les modifications faites dans le fichier Vagrantfile sont fonctionnelles.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Clearing any previously set forwarded ports...
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: Warning: Connection reset. Retrying...
    default: Warning: Connection aborted. Retrying...
    default: Warning: Connection reset. Retrying...
    default: Warning: Connection aborted. Retrying...
    default: Warning: Remote connection disconnect. Retrying...
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Setting hostname...
==> default: Configuring and enabling network interfaces...
```

On voit bien qu'il y a une carte sur le réseau Host-Only, et qu'il y a la mise en place du hostname.

Maintenant on veut modifier le Vagrantfile pour que la VM exécute un script shell au démarrage qui installe le paquet vim le Vagrantfile qui ajout un deuxième disque de 5Go à la VM.

La commande pour afficher le ficher Vagrantfile qui contient la modification pour la création du disque et l'éxécution du script qui installe VIM.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.hostname = "test"
  config.vm.provision :shell, path: "script_vim.sh"
  config.vm.define "test" do |ip|
    ip.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    ip.vm.provider "virtualbox" do |vb|
      vb.name = "test"
      vb.memory = "1024"
      disque = './secondDisk.vdi'
      unless File.exist?(disque)
        vb.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
      end
    vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
    end
  end
end
```

La commande qui affiche le script d'installation de VIM et qui indique l'adresse du DNS que l'on veut utiliser.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_vim.sh
#!/bin/bash
# Antoine Crenn
# 29/09/2020
# Installe vim
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
sudo yum install -y vim
```

Les lignes que l'on a ajouté pour avoir la création du disque supplémentaire de 5Go.

```ruby
disque = './secondDisk.vdi'
      unless File.exist?(disque)
        vb.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
      end
    vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
```

On démarre la VM avec la commande vagrant up pour voir si les modifications faites dans le fichier Vagrantfile sont fonctionnelles.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant up
Bringing machine 'test' up with 'virtualbox' provider...
==> test: Importing base box 'centos/7'...
==> test: Matching MAC address for NAT networking...
==> test: Setting the name of the VM: test
==> test: Clearing any previously set network interfaces...
==> test: Preparing network interfaces based on configuration...
    test: Adapter 1: nat
    test: Adapter 2: hostonly
==> test: Forwarding ports...
    test: 22 (guest) => 2222 (host) (adapter 1)
==> test: Running 'pre-boot' VM customizations...
==> test: Booting VM...
==> test: Waiting for machine to boot. This may take a few minutes...
    test: SSH address: 127.0.0.1:2222
    test: SSH username: vagrant
    test: SSH auth method: private key
    test:
    test: Vagrant insecure key detected. Vagrant will automatically replace
    test: this with a newly generated keypair for better security.
    test:
    test: Inserting generated public key within guest...
==> test: Machine booted and ready!
==> test: Checking for guest additions in VM...
    test: No guest additions were detected on the base box for this VM! Guest
    test: additions are required for forwarded ports, shared folders, host only
    test: networking, and more. If SSH fails on this machine, please install
    test: the guest additions and repackage the box to continue.
    test:
    test: This is not an error message; everything may continue to work properly,
    test: in which case you may ignore this message.
==> test: Setting hostname...
==> test: Configuring and enabling network interfaces...
==> test: Running provisioner: shell...
    test: Running: C:/Users/Crenn Antoine/AppData/Local/Temp/vagrant-shell20200929-38828-neqxg5.sh
    test: Loaded plugins: fastestmirror
    test: Determining fastest mirrors
    test:  * base: centos.mirror.ate.info
    test:  * extras: centos.mirror.ate.info
    test:  * updates: mirrors.standaloneinstaller.com
    test: Resolving Dependencies
    test: --> Running transaction check
    test: ---> Package vim-enhanced.x86_64 2:7.4.629-6.el7 will be installed
```

On voit que le package Vim a bien était installé.

Pour vérifier s'il y a eu l'ajout du disque on fait un ls -al de /dev/ car c'est pour le dossier pour les périphériques et on fait un grep pour trouvé les disques et on voit que l'on a le disque sdb donc il a bien marché.

```ruby
[vagrant@test ~]$ ls -al /dev/ | grep sd
brw-rw----.  1 root disk      8,   0 Sep 29 10:20 sda
brw-rw----.  1 root disk      8,   1 Sep 29 10:20 sda1
brw-rw----.  1 root disk      8,  16 Sep 29 10:20 sdb
```

### II. Re-emballer

Avec Vagrant on peut packager soi-même une box Vagrant afin d'avoir une VM sur-mesure pour ce faire on allume une VM de base, à l'interieur de la VM on s'occupe de la créations de fichier, de l'ajout de paquets, la configuration système par exemple, puis on quitte la VM et puis on utilise les commandes vagrant package --output centos7-custom.box et vagrant box add centos7-custom centos7-custom.box.

Sur notre VM de base on va faire les mises à jour des paquets, l'installation des paquets vim, epel-release, nginx, on va désactivé SELinux, configuré le firewall pour qu'il se lance au démarrage et qu'il laisse passé que le ssh.

La commande qui permet d'afficher le contenu du fichier Vagrantfile qui installe et configure une VM, exécute le script de mises à jour, d'installation, de désactivation de SELinux et de lancement du firewall.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.hostname = "test"
  config.vm.provision :shell, path: "script_installation.sh"
  config.vm.define "test" do |ip|
    ip.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    ip.vm.provider "virtualbox" do |vb|
      vb.name = "test"
      vb.memory = "1024"
    end
  end
end
```

Le script qui fait les mises à jour des paquets, installe epel-release, nginx et vim qui désactive SELinux et lance le firewall au boot.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> cat script_installation.sh
#!/bin/bash
# Antoine Crenn
# 29/09/2020
# Fait les mises à jour, installe epel-release, nginx, vim
# désactive le SELINUX et lance le firewall au boot.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
sudo yum update -y;
sudo yum install epel-release -y;
sudo yum install nginx -y;
sudo yum install -y vim
sudo setenforce 0;
sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;
sudo systemctl enable firewalld;
```

La commande qui permet de faire les mises à jour des paquets présents sur la VM.

```bash
sudo yum update -y
```

La commande qui permet d'installer le paquet epel-release pour ajouter des dêpots de paquets.

```bash
sudo yum install epel-release -y
```

La commande qui permet d'installer le paquet gninx pour pouvoir monter un serveur web.

```bash
sudo yum install nginx -y
```

La commande qui permet de désactiver SELinux jusqu'au prochain redémarrage.

```bash
sudo setenforce 0
```

La commande qui permet de modifier SELINUX=enforcing par SELINUX=permissive dans le fichier /etc/selinux/config.

```bash
sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;
```

La commande qui permet d'activer le firewall pour qu'il se lance au boot.

```bash
sudo systemctl enable firewalld
```

La comamnde qui permet de packager la VM où on a fait les mises à jour des paquets, l'installation des paquets vim, epel-release et nginx, désactiver SELinux et activé le firewall au boot. Pour pouvoir créer une box qui aura déjà ce que l'on a fait de configurer et d'installer.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant package --output b2-tp2-centos
==> test: Exporting VM...
==> test: Compressing package to: C:/Users/Crenn Antoine/Desktop/Cours/B2/Linux/Vagrant/b2-tp2-centos
```

La commande pour créer la box b2-tp2-centos grâce à la box que l'on vient de packager.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> vagrant box add centos7-custom b2-tp2-centos
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'b2-tp2-centos' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/Crenn%20Antoine/Desktop/Cours/B2/Linux/Vagrant/b2-tp2-centos
    box:
==> box: Successfully added box 'b2-tp2-centos' (v0) for 'virtualbox'!
```

### III. Déploiement multi-nœuds

On veut créer un Vragrantfile qui lance deux VMs qui utilise la box que l'on a créer un peu plus haut. La configuration de la première VM est une adresse IP locale qui est 192.168.2.21, le hostname qui est node1.tp2.b2, le nom Vagrant qui est node1 et qui a 1Go de RAM. La configuration de la seconde VM est une adresse IP locale qui est 192.168.2.22, le hostname qui est node2.tp2.b2, le nom Vagrant qui est node2 et qui a 512Mo de RAM.

La commande qui affiche le Vagrantfile qui créer la VM node 1 et node 2 et qui les configurées grâce à la VM repackagé dans la partie II.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "b2-tp2-centos"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision :shell, path: "script_dns.sh"
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.define "node1" do |node1_debut|
    node1_debut.vm.hostname = "node1.tp2.b2"
    node1_debut.vm.network "private_network", ip: "192.168.2.21", netmask:"255.255.255.0"
    node1_debut.vm.provider "virtualbox" do |node1|
      node1.name = "node1"
      node1.memory = "1024"
    end
  end
  config.vm.define "node2" do |node2_debut|
    node2_debut.vm.hostname = "node2.tp2.b2"
    node2_debut.vm.network "private_network", ip: "192.168.2.22", netmask:"255.255.255.0"
    node2_debut.vm.provider "virtualbox" do |node2|
      node2.name = "node2"
      node2.memory = "512"
    end
  end
end
```

La partie du Vagrantfile qui permet de créer et configurer la VM node1.

```ruby
config.vm.define "node1" do |node1_debut|
    node1_debut.vm.hostname = "node1.tp2.b2"
    node1_debut.vm.network "private_network", ip: "192.168.2.21", netmask:"255.255.255.0"
    node1_debut.vm.provider "virtualbox" do |node1|
      node1.name = "node1"
      node1.memory = "1024"
    end
  end
```

La partie du Vagrantfile qui permet de créer et configurer la VM node2.

```ruby
  config.vm.define "node2" do |node2_debut|
    node2_debut.vm.hostname = "node2.tp2.b2"
    node2_debut.vm.network "private_network", ip: "192.168.2.22", netmask:"255.255.255.0"
    node2_debut.vm.provider "virtualbox" do |node2|
      node2.name = "node2"
      node2.memory = "512"
    end
  end
```

### IV. L'automatisation ici nous venons (lentement)

Maintenant on veut automatiser la résolution du TP1 à l'aide de Vagrant et de scripts. Donc on veut créer un Vagrantfile qui automatise la résolution du TP1 pour ce faire on va mettre en place une VM serveur Web et une VM cliente qui ont les mêmes configurations que pour le TP1 sauf le partitionnement, les actions seront réalisées à l'aide de trois scripts qui se lancera au démarrage de la VM serveur et la VM client et des deux VMs.

Le fichier Vagrantfile pour la création des deux VMs et l'exécution des trois scripts.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "b2-tp2-centos"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.provision :shell, path: "script_deux_vm.sh"
  config.vm.define "node1" do |node1_debut|
    node1_debut.vm.hostname = "node1.tp2.b2"
    node1_debut.vm.network "private_network", ip: "192.168.2.21", netmask:"255.255.255.0"
    node1_debut.vm.provision :shell, path: "script_node1.sh"
    node1_debut.vm.provider "virtualbox" do |node1|
      node1.name = "node1"
      node1.memory = "1024"
      disque = './disque_node1.vdi'
      unless File.exist?(disque)
        node1.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
        node1.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
      end
    end
  end
  config.vm.define "node2" do |node2_debut|
    node2_debut.vm.hostname = "node2.tp2.b2"
    node2_debut.vm.network "private_network", ip: "192.168.2.22", netmask:"255.255.255.0"
    node2_debut.vm.provision :shell, path: "script_node2.sh"
    node2_debut.vm.provider "virtualbox" do |node2|
      node2.name = "node2"
      node2.memory = "512"
      disque = './disque_node2.vdi'
      unless File.exist?(disque)
        node2.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
        node2.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
      end
    end
  end
end
```

Le contenu du fichier Vagrantfile pour bloquer la vérification de mise à jour et ne pas les faire automatiquement, de mettre la clé ssh publique sur les deux VMs et de lancer le script qui s'exécute sur les deux VMs.

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.provision :shell, path: "script_deux_vm.sh"
```

Le contenu du fichier Vagrantfile pour faire la configuration de la VM node1, lui ajouté un disque de 5Go et lancer le script qui ne s'éxécute que sur la VM node1.

```ruby
config.vm.define "node1" do |node1_debut|
    node1_debut.vm.hostname = "node1.tp2.b2"
    node1_debut.vm.network "private_network", ip: "192.168.2.21", netmask:"255.255.255.0"
    node1_debut.vm.provision :shell, path: "script_node1.sh"
    node1_debut.vm.provider "virtualbox" do |node1|
      node1.name = "node1"
      node1.memory = "1024"
      disque = './disque_node1.vdi'
      unless File.exist?(disque)
        node1.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
        node1.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
      end
    end
  end
```

Le contenu du fichier Vagrantfile pour faire la configuration de la VM node2, lui ajouté un disque de 5Go et lancer le script qui ne s'éxécute que sur la VM node2.

```ruby
  config.vm.define "node2" do |node2_debut|
    node2_debut.vm.hostname = "node2.tp2.b2"
    node2_debut.vm.network "private_network", ip: "192.168.2.22", netmask:"255.255.255.0"
    node2_debut.vm.provision :shell, path: "script_node2.sh"
    node2_debut.vm.provider "virtualbox" do |node2|
      node2.name = "node2"
      node2.memory = "512"
      disque = './disque_node2.vdi'
      unless File.exist?(disque)
        node2.customize ['createhd', '--filename', disque, '--variant', 'Fixed', '--size', 5120]
        node2.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disque]
      end
    end
  end
```

Le script de configuration qui s'éxécute sur les deux VMs.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_deux_vm.sh
#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Script pour la partie de configuration commune aux deux VMs.

# Permet de définir le serveur DNS que l'on veut utiliser.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf

# Les variables qui seront utilisées dans le script.
utilisateur="admin";
mot_de_passe="un_mot_de_passe"

# Permet de créer un utilisateur admin qui a un home.

sudo useradd $utilisateur -m;

# Permet d'ajouter un mot de passe à l'utilisateur admin.

echo -e "$mot_de_passe\n$mot_de_passe\n" | sudo passwd $utilisateur

# Permet d'ajouter l'utilisateur admin aux utilisateurs qui peuvent faire des commandes sudo.

sudo echo "$utilisateur   ALL=(ALL)       ALL" >> /etc/sudoers
```

Le script de configuration qui s'exécute que sur la VM node1.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_node1.sh
#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Script de configuration de la VM node1 que ça soit les sites,
# le backup et le netdata.

# Les différentes variables utilisées dans le script.
chemin_srv="/srv";
chemin_site1="${chemin_srv}/site1";
chemin_site2="${chemin_srv}/site2";
nom_fichier="index.html";
nom_cle="site.key";
nom_certificat="site.crt";
url="https:\/\/discord.com\/api\/webhooks\/761268162905767946\/a9ubvnEhLd7wQvmmScZ1kU-wbmsXwijidKzSs8op6ZQIeH5kXdqwwlFkMwJm24IRUpD6";

# La création des utilisateurs pour le script les deux ne peuvent pas se connecter,
# l'utilisateur web n'a pas de d'home et l'utilisateur backup à un home et appartient au groupe web.

sudo useradd web -M -s /sbin/nologin;

sudo useradd -m backup -G web -s /sbin/nologin;

# Créer les chemins des deux sites s'ils n'existent pas.

if test ! -d ${chemin_site1};
then
  sudo mkdir ${chemin_site1};
fi

if test ! -d ${chemin_site2};
then
  sudo mkdir ${chemin_site2};
fi

# Permet de mettre seulement les autorisations de lecture, d'écriture et d'éxécution
# à l'utilisateur et au groupe sur les dossiers des deux sites.

sudo chmod 550 ${chemin_site1} ${chemin_site2};

# Associe l'adresse IP de la VM node2 au nom de la VM node2.

sudo echo "192.168.2.22  node2.tp2.b2" >> /etc/hosts;

# Remplir le fichier index.html du site 1.

sudo echo "<!DOCTYPE html>
<html lang=\"fr\">

<head>
  <meta charset=\"utf-8\">
  <title>Page Accueil</title>
  <meta name=\"description\" content=\"Page Accueil\" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>" > ${chemin_site1}/${nom_fichier};

# Remplir le fichier index.html du site 2.

sudo echo "<!DOCTYPE html>
<html lang=\"fr\">

<head>
  <meta charset=\"utf-8\">
  <title>Page Accueil</title>
  <meta name=\"description\" content=\"Page Accueil\" />
</head>

<body>
 <h1> Coucou vous etez sur le site 2</h1>
</body>" > ${chemin_site2}/${nom_fichier};

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur
# et au groupe sur les fichiers index.html des deux sites.

sudo chmod 440 ${chemin_site1}/${nom_fichier} ${chemin_site2}/${nom_fichier};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire des fichier index.html
# des deux sites pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_site1}/${nom_fichier} ${chemin_site2}/${nom_fichier};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire des dossiers
# des deux sites pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_site1}/ ${chemin_site2}/;

# Permet de créer la clé pour que les sites soient en https.

echo "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDfahs/nVslUswA
ro8KCyonBmqi73Dwby1gqVAQvycVNqBWgNjtWsy5bA/No+6WJp0o1Juajx/E2uZu
hshQZhhuOmUhVQNLZXA9IUqKdOmSNxl8W0zsECz5/6AnY2Ecglc02+1SLYJdx2nr
yjVo2r+jnEOkln+zI6Pn8JSVXlDqHO5/8F8+3NpJGbXrSQC15yNFMMAepmfwVypM
wU34ASNLFLkZnxhWy8Ta5e8+YNTY3Y7uUO9yqgGG5vu4V0BFkL8aoqpf5ky5+jje
K4VhHp1o3p1V/XTaQYhAzGuGdIpYcIYo02Lmvlra/6EyESlmTrKl83Zto5MXv8JR
LCQ4eFMxAgMBAAECggEANI5odJGW1HPiy7JEse8obl/DDCtZnSX6Pcf2rsnY7tOX
6mGLJmL60A/Ms+qBgM76n5VMOZK7vXplRNDuJIOv8ZZJHgs6qCznt2myeFnCbIQL
ucZclRfFDNaGnV1yhdtfmm62KGPOTDNSBobeA5maBWoIO5qv1btnnnNo8K5sSHYv
WgC3lsOevKAc7DbT0gSvqFfs5Jfqvuw8DKNtiq6kUjmy9Pp1HHyFbUvTJnfjduXz
ysRCVw1AxcZl2z6teNeDEYefTAO7ljkE2vUtx+yCgOI19i4DE0f81iIllzZIwAfk
tsDm6KZkjxULBl0n0JFA02DuY+/eDGhqRsn8ePaGcQKBgQDyBxZpIVUrduwFSf96
9N8gPPAfizPtG3NGT62H2W9XUtQOhmMErf/CeTEiWSnUkxW2IMWuPnh5eju4UKYi
rhl2QgncjLTDiwb/jImpLJDMDyMvZbkCHHXjrpQyZkeJ/7/bOjrdWdXMDivR4veF
ub6255VI/CZl8Da4G0XTlda1XQKBgQDsT+94wYBaDFTzqL88Lfw2Bp2P0BeLgXd+
SOeResY4BV7I6vxHWg/XpK/T+xmC+XT56wRewXWZM7ii8Jdy6pdRMp40Gfr2Mj6T
iNOt/eKlVzJB+n8cDuS7KytCWiVlmUonAsK8wMcP3P2emYEeeRXuBHH5fQIzHG95
Lb4Cm+gD5QKBgQCMQoc+Tc7p7XVCa2vfeeGE8UvHPAjPtETNzocmX2AjHyE9eawQ
njXGIi3TVhDj3ImMjtmA7TssSyYo3rQpDPQ8kHDem7x2Ri+8EVoF7InNeEhPw4fA
LXY0iIvh2VxEIS6ygcBzXwiDxQKoE/yWRtq7bOIa9eXa1acgcs24nPFdEQKBgQCL
YMI/mPGZKvjYFcllN0Iwo0o/Tl70u1G9PEbIuX87/37eRiqx8t6xM8M45jsv0u45
kXIsaTJxP95AzGKnNtI05AQHeuNYXl4mKmefkcUh/AH6hEKATBCAjH+hTQYNnJHt
3GT7a0B9jLxR1Pwg+w4CKsEGg8LOXpEpCd+uLkJ0OQKBgAl2T4dmu3WlUuT0Va0b
EChtSyf96dzYpqMk8wcskfTloUYCnQSLDvjoAzQMVUrf2RQ30VPji+V5fgHnE5i2
KcIW+2OVvcHFJCYq9Lm5qJMmNRa/F1Csm9r8jjTELy8Oj0sqmxmCFNBFyogECMh3
+oUknuTGb5GGxRhAYtToNOzm
-----END PRIVATE KEY-----" > /srv/site.key;

# Permet de créer le certificat ssl pour que les sites soient en https.

echo "-----BEGIN CERTIFICATE-----
MIIDATCCAemgAwIBAgIJALdc9e7u6JNuMA0GCSqGSIb3DQEBCwUAMBcxFTATBgNV
BAMMDG5vZGUxLnRwMi5iMjAeFw0yMDEwMDMxMjU5MTBaFw0yMTEwMDMxMjU5MTBa
MBcxFTATBgNVBAMMDG5vZGUxLnRwMi5iMjCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBAN9qGz+dWyVSzACujwoLKicGaqLvcPBvLWCpUBC/JxU2oFaA2O1a
zLlsD82j7pYmnSjUm5qPH8Ta5m6GyFBmGG46ZSFVA0tlcD0hSop06ZI3GXxbTOwQ
LPn/oCdjYRyCVzTb7VItgl3HaevKNWjav6OcQ6SWf7Mjo+fwlJVeUOoc7n/wXz7c
2kkZtetJALXnI0UwwB6mZ/BXKkzBTfgBI0sUuRmfGFbLxNrl7z5g1Njdju5Q73Kq
AYbm+7hXQEWQvxqiql/mTLn6ON4rhWEenWjenVX9dNpBiEDMa4Z0ilhwhijTYua+
Wtr/oTIRKWZOsqXzdm2jkxe/wlEsJDh4UzECAwEAAaNQME4wHQYDVR0OBBYEFKkr
s/LIJ5cMnG+O7oAgsnKqLugCMB8GA1UdIwQYMBaAFKkrs/LIJ5cMnG+O7oAgsnKq
LugCMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAKcQpusq3DpcVa1d
ZWpukdkL5NFEPsBUkMU6nLKUhmV0IV+GuwC6DVSzCPsohGy1FyuMRsKUst4rbRie
KbwRXdNYGJQCKxdqbqAaDeu/zIf1Srz+ylnq776+J+tjnSWFzTrdPhCeQX0cuoIJ
HhwE2U4gKZr8qhJiXpwVlF1WhNu0wFF1dkTyj5T21rYpuBF792Jj38tJaogTAmPD
zQEKRqRUaITYeM/puIvHwWrccHoxLiY058b20+XFLH4vYNSAiBkZULh4zkNMap+s
hZ3tznir1QUrF+0IEseLzRAuPe0H6L2tAaBXDa16QEzYE/brHgttC/b5zpk0zNhn
q/7b6k0=
-----END CERTIFICATE-----" > /srv/site.crt;

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur
# sur la clé du certificat.

sudo chmod 400 ${chemin_srv}/${nom_cle};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire de la clé
# pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_srv}/${nom_cle};

# Permet de mettre seulement l'autorisation de lecture à l'utilisateur,
# au groupe et aux autres utilisateurs sur le certificat.

sudo chmod 444 ${chemin_srv}/${nom_certificat};

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du certificat
# pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire.

sudo chown web:web ${chemin_srv}/${nom_certificat};

# Permet de définir la configuration pour les deux sites pour qu'ils soient en http et https
# et qu'ils soient relier à netdata.

sudo echo "worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;

events {
    worker_connections 1024;
}

http {

      upstream netdata {
        server 127.0.0.1:19999;
        keepalive 64;
    }

    server {
        listen 80;
        server_name node1.tp2.b2;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias ${chemin_site1}/${nom_fichier};
        }

        location /site2 {
            alias ${chemin_site2}/${nom_fichier};
        }

        location = /netdata {
            return 301 /netdata/;
        }

        location ~ /netdata/(?<ndpath>.*) {
            proxy_redirect off;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-Host \$host;
            proxy_set_header X-Forwarded-Server \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_pass_request_headers on;
            proxy_set_header Connection \"keep-alive\";
            proxy_store off;
            proxy_pass http://netdata/\$ndpath\$is_args\$args;
            gzip on;
            gzip_proxied any;
            gzip_types *;
        }
    }
    server {
        listen 443 ssl;

        server_name node1.tp2.b2;
        ssl_certificate ${chemin_srv}/${nom_certificat};
        ssl_certificate_key ${chemin_srv}/${nom_cle};

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias ${chemin_site1}/${nom_fichier};
        }

        location /site2 {
            alias ${chemin_site2}/${nom_fichier};
        }

        location = /netdata {
            return 301 /netdata/;
        }

        location ~ /netdata/(?<ndpath>.*) {
            proxy_redirect off;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-Host \$host;
            proxy_set_header X-Forwarded-Server \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_pass_request_headers on;
            proxy_set_header Connection \"keep-alive\";
            proxy_store off;
            proxy_pass http://netdata/\$ndpath\$is_args\$args;
            gzip on;
            gzip_proxied any;
            gzip_types *;
        }
    }
}" > /etc/nginx/nginx.conf;

# Permet d'ouvrir le port 80/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=80/tcp --permanent;

# Permet d'ouvrir le port 443/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=443/tcp --permanent;

# Permet d'ouvrir le port 19999/tcp au niveau du firewall de façon permanent.

sudo firewall-cmd --add-port=19999/tcp --permanent

# Permet de rédemarré le firewall avec les modifications de port.

sudo firewall-cmd --reload;

# Le script de backup qui permet de sauvegarder le site qui est passé en argument.

sudo echo -e "#!/bin/bash
# Antoine Crenn
# 01/10/2020
# Script Sauvegarde
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
nom_sauvegarde=\"\${nom_site}_\${jour}_\${heure}\";
chemin_fermer=\"/home/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/home/backup/\";

if test -z \${chemin_site};
then
        echo \"Vous avez oublié de rentrer l'argument.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${chemin_site};
then
        echo \"Le chemin du site \${chemin_site} n'existe pas.\" >&2;
        exit 1;
fi

if test -f \${chemin_fermer};
then
        exit 22;
fi

touch \${chemin_fermer};

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_site} --ignore-failed-read > /dev/null;

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
        exit 1;
fi

nombre_sauvegarde=\$(ls \${chemin_sauvegarde} | grep -c \${nom_site});

if test \${nombre_sauvegarde} -ge 9;
then
        supprimer=\$(ls \${chemin_sauvegarde} | grep \${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf \${chemin_sauvegarde}\${supprimer};
fi

rm -rf \${chemin_fermer};" > /home/backup/tp1_backup.sh;

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du script de backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /home/backup/tp1_backup.sh;

# Permet de mettre seulement les autorisations de lecture, d'éciture et d'éxécution
# à l'utilisateur sur le fichier tp1_backup.sh.

sudo chmod 700 /home/backup/tp1_backup.sh;

# Permet de faire que le script de backup se lance toutes les heures pour les deux sites.

sudo echo "  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site1

  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site2" >> /etc/crontab;

# Permet d'installer Netdata sans devoir intervenir lors de l'installation.

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

# Permet au système de trouvé les fichiers de configuration netdata.

/etc/netdata/edit-config health_alarm_notify.conf;

# Permet d'indiquer que l'on veut recevoir des alarmes grâce à Discord.

sudo sed -i "s/DEFAULT_RECIPIENT_DISCORD=\"\"/DEFAULT_RECIPIENT_DISCORD=\"alarms\"/" /etc/netdata/health_alarm_notify.conf;

# Permet d'ajouter l'URL du WEBHOOK discord pour que l'on reçoit les notications pour le site sur un salon Discord dédié.

sudo sed -i "s/DISCORD_WEBHOOK_URL=\"\"/DISCORD_WEBHOOK_URL=\"${url}\"/" /etc/netdata/health_alarm_notify.conf;

# Permet de lancer nginx avec les modifications de configuration.

sudo systemctl restart nginx;
```

Le script de configuration qui s'exécute que sur la VM node2.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_node2.sh
#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Le script de la configuration pour la VM node2.

# Associe l'adresse IP de la VM node1 au nom de la VM node1.
sudo echo "192.168.2.21  node1.tp2.b2" >> /etc/hosts;

# Permet de créer le certificat ssl pour que les sites de la VM node1 soient directement
# validés par la VM node2.

echo "-----BEGIN CERTIFICATE-----
MIIDATCCAemgAwIBAgIJALdc9e7u6JNuMA0GCSqGSIb3DQEBCwUAMBcxFTATBgNV
BAMMDG5vZGUxLnRwMi5iMjAeFw0yMDEwMDMxMjU5MTBaFw0yMTEwMDMxMjU5MTBa
MBcxFTATBgNVBAMMDG5vZGUxLnRwMi5iMjCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBAN9qGz+dWyVSzACujwoLKicGaqLvcPBvLWCpUBC/JxU2oFaA2O1a
zLlsD82j7pYmnSjUm5qPH8Ta5m6GyFBmGG46ZSFVA0tlcD0hSop06ZI3GXxbTOwQ
LPn/oCdjYRyCVzTb7VItgl3HaevKNWjav6OcQ6SWf7Mjo+fwlJVeUOoc7n/wXz7c
2kkZtetJALXnI0UwwB6mZ/BXKkzBTfgBI0sUuRmfGFbLxNrl7z5g1Njdju5Q73Kq
AYbm+7hXQEWQvxqiql/mTLn6ON4rhWEenWjenVX9dNpBiEDMa4Z0ilhwhijTYua+
Wtr/oTIRKWZOsqXzdm2jkxe/wlEsJDh4UzECAwEAAaNQME4wHQYDVR0OBBYEFKkr
s/LIJ5cMnG+O7oAgsnKqLugCMB8GA1UdIwQYMBaAFKkrs/LIJ5cMnG+O7oAgsnKq
LugCMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAKcQpusq3DpcVa1d
ZWpukdkL5NFEPsBUkMU6nLKUhmV0IV+GuwC6DVSzCPsohGy1FyuMRsKUst4rbRie
KbwRXdNYGJQCKxdqbqAaDeu/zIf1Srz+ylnq776+J+tjnSWFzTrdPhCeQX0cuoIJ
HhwE2U4gKZr8qhJiXpwVlF1WhNu0wFF1dkTyj5T21rYpuBF792Jj38tJaogTAmPD
zQEKRqRUaITYeM/puIvHwWrccHoxLiY058b20+XFLH4vYNSAiBkZULh4zkNMap+s
hZ3tznir1QUrF+0IEseLzRAuPe0H6L2tAaBXDa16QEzYE/brHgttC/b5zpk0zNhn
q/7b6k0=
-----END CERTIFICATE-----" > /etc/pki/ca-trust/source/anchors/site.crt;

# Permer de mettre à jour les certificats qui sont validés par la VM node2.

update-ca-trust;
```