#!/bin/bash
# Antoine Crenn
# 04/10/2020
# Le script de la configuration pour la VM node2.

# Associe l'adresse IP de la VM node1 au nom de la VM node1.
sudo echo "192.168.2.21  node1.tp2.b2" >> /etc/hosts;

# Permet de créer le certificat ssl pour que les sites de la VM node1 soient directement
# validés par la VM node2.

echo "-----BEGIN CERTIFICATE-----
MIIDATCCAemgAwIBAgIJALdc9e7u6JNuMA0GCSqGSIb3DQEBCwUAMBcxFTATBgNV
BAMMDG5vZGUxLnRwMi5iMjAeFw0yMDEwMDMxMjU5MTBaFw0yMTEwMDMxMjU5MTBa
MBcxFTATBgNVBAMMDG5vZGUxLnRwMi5iMjCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBAN9qGz+dWyVSzACujwoLKicGaqLvcPBvLWCpUBC/JxU2oFaA2O1a
zLlsD82j7pYmnSjUm5qPH8Ta5m6GyFBmGG46ZSFVA0tlcD0hSop06ZI3GXxbTOwQ
LPn/oCdjYRyCVzTb7VItgl3HaevKNWjav6OcQ6SWf7Mjo+fwlJVeUOoc7n/wXz7c
2kkZtetJALXnI0UwwB6mZ/BXKkzBTfgBI0sUuRmfGFbLxNrl7z5g1Njdju5Q73Kq
AYbm+7hXQEWQvxqiql/mTLn6ON4rhWEenWjenVX9dNpBiEDMa4Z0ilhwhijTYua+
Wtr/oTIRKWZOsqXzdm2jkxe/wlEsJDh4UzECAwEAAaNQME4wHQYDVR0OBBYEFKkr
s/LIJ5cMnG+O7oAgsnKqLugCMB8GA1UdIwQYMBaAFKkrs/LIJ5cMnG+O7oAgsnKq
LugCMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAKcQpusq3DpcVa1d
ZWpukdkL5NFEPsBUkMU6nLKUhmV0IV+GuwC6DVSzCPsohGy1FyuMRsKUst4rbRie
KbwRXdNYGJQCKxdqbqAaDeu/zIf1Srz+ylnq776+J+tjnSWFzTrdPhCeQX0cuoIJ
HhwE2U4gKZr8qhJiXpwVlF1WhNu0wFF1dkTyj5T21rYpuBF792Jj38tJaogTAmPD
zQEKRqRUaITYeM/puIvHwWrccHoxLiY058b20+XFLH4vYNSAiBkZULh4zkNMap+s
hZ3tznir1QUrF+0IEseLzRAuPe0H6L2tAaBXDa16QEzYE/brHgttC/b5zpk0zNhn
q/7b6k0=
-----END CERTIFICATE-----" > /etc/pki/ca-trust/source/anchors/site.crt;

# Permer de mettre à jour les certificats qui sont validés par la VM node2.

update-ca-trust;
