#!/bin/bash
# Antoine Crenn
# 09/10/2020
# Permet de mettre en place le service web et le service de backup.

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
# Créer l'utilisateur web qui n'a pas de home et qui ne peut pas se connecter.
sudo useradd web -M -s /sbin/nologin;
# Créer l'utilisateur BACKUP qui a un home et qui ne peut pas se connecter.
sudo useradd backup -m -s /sbin/nologin;

# Rempli le fichier qui va permettre de créer le service web.

sudo echo "[Unit]
# Permet de rédiger la description du service web.
Description=Service Serveur Web Python2

[Service]
# Permet de scpécifier qu'elle utilisateur lance le service.
User=web
# Mets le port en variable d'environnement.
Environment=\"PORT=2402\"
# Permet d'ouvrir le port avant de lancer le serveur web.
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=\${PORT}/tcp
# Lance le serveur web.
ExecStart=/usr/bin/python2 -m SimpleHTTPServer \${PORT}
# Permet de fermer le port après la fermeture du serveur web.
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=\${PORT}/tcp

[Install]
# Permet d'indiquer que le service a la possibilité d'être lancé au démarrage de la machine.
WantedBy=multi-user.target" > /etc/systemd/system/web.service

# Permet d'ajouter les droits sudo à l'utilisateur indiquer sans
# qu'il est besoin de rentrer de mot de passe.

sudo echo "web     ALL=(ALL)       NOPASSWD: ALL
backup     ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Rempli le fichier qui va permettre de créer le service backup.

sudo echo "[Unit]
# Permet de rédiger la description du service backup.
Description=Service de Backup pour site

[Service]
# Permet de scpécifier qu'elle utilisateur lance le service.
User=backup
# Permet d'indiquer quel est le PIDfile du service.
PIDFile=/opt/backup/backup.pid
# Permet de lancer le script qui permet de faire les tests avant la sauvegarde.
ExecStartPre=/bin/bash /opt/test_sauvegarde.sh /srv
# Permet de lancer le script de sauvegarde.
ExecStart=/bin/bash /opt/sauvegarde.sh /srv
# Permet de lancer le script qui supprimer la sauvegarde la plus ancienne
# au bout d'un certain nombre.
ExecStartPost=/bin/bash /opt/nombre_sauvegarde.sh /srv" > /etc/systemd/system/backup.service

# Permet de remplir la partie du script de backup qui fait les tests.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script Test pour la Sauvegarde
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
chemin_fermer=\"/opt/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/opt/backup/\";

if test -z \${chemin_site};
then
        echo \"Vous avez oublié de rentrer l'argument.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${chemin_site};
then
        echo \"Le chemin du site \${chemin_site} n'existe pas.\" >&2;
        exit 1;
fi

if test -f \${chemin_fermer};
then
        exit 22;
fi

touch \${chemin_fermer};" > /opt/test_sauvegarde.sh

# Permet de remplir la partie du script de backup qui fait la sauvegarde
# et remplir le numéro du processus dans le PIDFile.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script de Sauvegarde
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
nom_sauvegarde=\"\${nom_site}_\${jour}_\${heure}\";
chemin_sauvegarde=\"/opt/backup/\";

echo \$\$ | sudo tee /opt/backup/backup.pid

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_site} --ignore-failed-read > /dev/null;

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
        exit 1;
fi" > /opt/sauvegarde.sh

# Permet de remplir la partie du script de backup qui supprimer la dernière sauvegarde
# s'il y en a plus de 7 du même dossier.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script vérification du nombre de Sauvegarde
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
chemin_fermer=\"/opt/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/opt/backup/\";

nombre_sauvegarde=\$(ls \${chemin_sauvegarde} | grep -c \${nom_site});

if test \${nombre_sauvegarde} -ge 9;
then
        supprimer=\$(ls \${chemin_sauvegarde} | grep \${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf \${chemin_sauvegarde}\${supprimer};
fi

rm -rf \${chemin_fermer};" > /opt/nombre_sauvegarde.sh

# Créer un dossier backup dans le dossier opt, c'est l'endroit
# où l'on va enregistrer les sauvegarde faites par le service backup.

sudo mkdir /opt/backup

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /opt/backup

# Rempli le fichier backup.timer qui permet que le service backup

sudo echo "[Unit]
# Permet de rédiger la description du timer backup.
Description=Sauvegarde toutes les heures grace au service backup

[Timer]
# Indique que l'on veut que la répétion du timer soit toutes les heures.
OnCalendar=hourly
# Permet de lancer le service si la dernière exécution a été manquer.
Persistent=true

[Install]
# Permet d'indiquer que le timer a la possibilité d'être lancé au démarrage de la machine.
WantedBy=timers.target" > /etc/systemd/system/backup.timer

# Indique au système qu'il faut relire les fichiers ce qui permet qu'il trouve
# le ou les services que l'on a ajouté.

sudo systemctl daemon-reload

# Permet que le service web soit lancé au boot de la machine.

sudo systemctl enable web.service

# Permet de lancer le timer pour le service backup.

sudo systemctl start backup.timer

# Permet de lancer la minuterie pour le service backup au démarrage de la machine.

sudo systemctl enable backup.timer
