# B2 Linux 2020 - TP3

## TP3: systemd

### 0. Prérequis

On va créer une VM grâce à Vagrant à partir de la box Centos/7. Grâce à un script d'installation on va installer epel-release, nginx, vim et on sera les mises à jours des paquets. Et après on va repackagé la VM que l'on vient de créer.

La commande qui permet d'afficher le contenu du fichier Vagrantfile qui installe et configure une VM, exécute le script de mises à jour, d'installation, de désactivation de SELinux et de lancement du firewall.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.hostname = "test"
  config.vm.provision :shell, path: "script_installation.sh"
  config.vm.define "test" do |ip|
    ip.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    ip.vm.provider "virtualbox" do |vb|
      vb.name = "test"
      vb.memory = "1024"
    end
  end
end
```

[Vagrantfile du prérequis](./Vagrantfile_prerequis)

Le script qui fait les mises à jour des paquets, installe epel-release, nginx et vim qui désactive SELinux et lance le firewall au boot.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\Vagrant> cat script_installation.sh
#!/bin/bash
# Antoine Crenn
# 29/09/2020
# Fait les mises à jour, installe epel-release, nginx, vim
# désactive le SELINUX et lance le firewall au boot.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
sudo yum update -y;
sudo yum install epel-release -y;
sudo yum install nginx -y;
sudo yum install -y vim
sudo setenforce 0;
sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;
sudo systemctl enable firewalld;
```

[Script qui permet les installations](./script/script_installation.sh)

La commande qui permet de faire les mises à jour des paquets présents sur la VM.

```bash
sudo yum update -y
```

La commande qui permet d'installer le paquet epel-release pour ajouter des dêpots de paquets.

```bash
sudo yum install epel-release -y
```

La commande qui permet d'installer le paquet gninx pour pouvoir monter un serveur web.

```bash
sudo yum install nginx -y
```

La commande qui permet de désactiver SELinux jusqu'au prochain redémarrage.

```bash
sudo setenforce 0
```

La commande qui permet de modifier SELINUX=enforcing par SELINUX=permissive dans le fichier /etc/selinux/config.

```bash
sudo sed -i "s/SELINUX=enforcing/SELINUX=permissive/" /etc/selinux/config;
```

La commande qui permet d'activer le firewall pour qu'il se lance au boot.

```bash
sudo systemctl enable firewalld
```

La commande pour packager la VM où on a installé epel-release, nginx, vim et on a fait les mises à jour de paquets.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> vagrant package --output centos7_tp3.box
==> test: Attempting graceful shutdown of VM...
==> test: Clearing any previously set forwarded ports...
==> test: Exporting VM...
==> test: Compressing package to: C:/Users/Crenn Antoine/Desktop/Cours/B2/Linux/vagrant/centos7_tp3.box
```

La commande pour créer la box centos7 avec les paquets epel-release, nginx et vim grâce à la box que l'on vient de packager.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> vagrant box add centos7-custom centos7_tp3.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos7-custom' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/Crenn%20Antoine/Desktop/Cours/B2/Linux/vagrant/centos7_tp3.box
    box:
==> box: Successfully added box 'centos7_tp3.box' (v0) for 'virtualbox'!
```

### I. Système de servicesd

La commande qui permet d'afficher le contenu du fichier Vagrantfile qui utilise la VM repackagé et exécute le script qui va configurer les différents services.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos7-custom"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.define "test" do |test_debut|
    test_debut.vm.hostname = "test"
    test_debut.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    test_debut.vm.provision :shell, path: "script_service.sh"
    test_debut.vm.provider "virtualbox" do |test|
      test.name = "test"
      test.memory = "1024"
    end
  end
end
```

[Vagrantfile de la Partie 1](./Vagrantfile_partie1)

#### 1. Introduction

On veut utiliser la ligne de commande pour afficher le nombre de services systemd disponibles sur la machine pour ce faire on fait la commande systemctl -t service -all | grep -c service qui permet d'afficher tous les services et les compte.

```bash
[vagrant@test ~]$ systemctl -t service -all | grep -c service
108
```

Le résultat de la commande nous montre qu'il y a 108 services disponibles sur la machine.

On veut utiliser la ligne de commande pour afficher le nombre de services systemd qui tourent sur la machine pour ce faire on fait la commande systemctl -t service -all | grep -c running qui permet d'afficher les services qui tournent sur la machine et les compte.

```bash
[vagrant@test ~]$ systemctl -t service -all | grep -c running
17
```

Le résultat de la commande nous montre qu'il y a 17 services qui tournent sur la machine.

On veut utiliser la ligne de commande pour afficher le nombre de services systemd qui ont soit échouer soit finir sur la machine pour ce faire on fait la commande systemctl -t service -all | grep -c -e failed -e exit qui permet d'afficher les services qui ont soit échouer soit finir sur la machine et les compte.

```bash
[vagrant@test ~]$ systemctl -t service -all | grep -c -e failed -e exit
17
```

Le résultat de la commande nous montre qu'il y a 17 services qui sont soit finir, soit échouer sur la machine.

On veut utiliser la ligne de commande pour afficher le nombre de services systemd qui sont lancés au boot sur la machine pour ce faire on fait la commande systemctl list-unit-files -t service -all | grep -c enabled qui permet d'afficher les services qui sont lancés au boot sur la machine et les compte.

```bash
[vagrant@test ~]$ systemctl list-unit-files -t service -all | grep -c enabled
32
```

Le résultat de la commande nous montre qu'il y a 32 services qui se lancent au boot sur la machine.

#### 2. Analyser un service

On veut édudier le service nginx.service pour ce faire on fait la commande systemctl cat nginx.service qui nous permet d'afficher le contenu du fichier qui permet de lancer le service nginx.service.

```bash
[vagrant@test ~]$ systemctl cat nginx.service
# /usr/lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

Le chemin de l'unité nginx.service est /usr/lib/systemd/system/nginx.service. La ligne ExecStart est le fichier exécutable principale qui est lancé par le service. Les lignes ExecStartPre sont des commandes qui doivent être exécutés pour permettre que la ligne ExecStart marche. La ligne ExecReload indique la commande qui doit être exécuté lorsqu'il y a un redémarrage du service c'est généralement un signal et $MAINPID est une variable d'environnement liée au processus. La ligne PIDfile précise l'emplacement du fichier PID qui là est dans le dossier run. La ligne Type indique le type de service que c'est. La ligne Description permet de rédiger une description du service. La ligne After indique les services qu'il faut obligatoirement pour que le service se lance.

On cherche à lister tous les services qui contiennent la ligne WantedBy=multi-user.target.

```bash
[vagrant@test ~]$ grep -r "WantedBy=multi-user.target" /etc/systemd/system/* /run/systemd/system/* /usr/lib/systemd/system/*| cut -d':' -f1 | cut -d'/' -f5,6 | cut -d'/' -f2
dbus-org.fedoraproject.FirewallD1.service
auditd.service
brandbot.path
chronyd.service
chrony-wait.service
cpupower.service
crond.service
ebtables.service
firewalld.service
fstrim.timer
gssproxy.service
irqbalance.service
machines.target
NetworkManager.service
nfs-client.target
nfs-rquotad.service
nfs-server.service
nfs.service
nginx.service
postfix.service
rdisc.service
remote-cryptsetup.target
remote-fs.target
rhel-configure.service
rpcbind.service
rpc-rquotad.service
rsyncd.service
rsyslog.service
sshd.service
tcsd.service
tuned.service
vmtoolsd.service
wpa_supplicant.service
```

Pour ce faire on a cherché dans tous les chemins où sont présents les différents services on a pus les voir grâce à un man de systemd.unit puis après on fait un cut où le séparateur est les deux points et on prend le premier champ puis on refait un cut où on le séparateur est un slash et on prend le 5ème et 6ème champs puis on refait un cut où le séparateur est un slash et on prend le deuxième champ.

#### 3. Création d'un service

Pour la création d'un service il suffit de créer un fichier au bon endroit avec une syntaxe particulière. L'endroit où l'on place géneralement ces fichiers est /etc/systemd/system/ et après il faut faire la commande sudo systemctl daemon-reload pour que le système relie tous les fichiers et trouve le service que l'on vient d'ajouter.

##### A. Serveur Web

Maintenant on veut mettre en place un service web qui utilise python pour lancer le serveur web, on veut que le port s'ouvre juste avant le serveur web, on veut que le port se referme juste après la fermeture du serveur, on veut aussi que ça soit un utilisateur dédié qui lance le service web, que le service web contient une description et que le port utilisé soit contenu dans une variable d'environnement.

Pour ce faire on a créer un script qui excécuté toutes les commandes à savoir la création de l'utilisateur, lui donner les permissions pour qu'il puisse passé des commandes sudo, remplir le fichier qui permet de lancé le service web, qui demande au système de relire tous les fichiers et lance le service au boot de la machine.

La commande qui permet d'afficher le contenu du script qui permet d'exécuter toutes les commandes pour la mise en place du service web.

```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_service.sh
#!/bin/bash
# Antoine Crenn
# 09/10/2020
# Permet de mettre en place le service web.

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
# Créer l'utilisateur web qui n'a pas de home et qui ne peut pas se connecter.
sudo useradd web -M -s /sbin/nologin;

# Rempli le fichier qui va permettre de créer le service web.

sudo echo "[Unit]
# Permet de rédiger la description du service web.
Description=Service Serveur Web Python2

[Service]
# Permet de scpécifier quel utilisateur lance le service.
User=web
# Mets le port en variable d'environnement.
Environment=\"PORT=2402\"
# Permet d'ouvrir le port avant de lancer le serveur web.
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=\${PORT}/tcp
# Lance le serveur web.
ExecStart=/usr/bin/python2 -m SimpleHTTPServer \${PORT}
# Permet de fermer le port après la fermeture du serveur web.
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=\${PORT}/tcp

[Install]
# Permet d'indiquer que le service a la possibilité d'être lancé au démarrage de la machine.
WantedBy=multi-user.target" > /etc/systemd/system/web.service

# Permet d'ajouter les droits sudo à l'utilisateur indiquer sans
# qu'il est besoin de rentrer de mot de passe.

sudo echo "web     ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Indique au système qu'il faut relire les fichiers ce qui permet qu'il trouve
# le ou les services que l'on a ajouté.

sudo systemctl daemon-reload

# Permet que le service web soit lancé au boot de la machine.

sudo systemctl enable web.service
```

[Script qui créer les services](./script/script_service.sh)

Maintenant on cherche à vérifier que le service web qui est mis en place grâce au script fonctionne bien, pour ce faire on veut vérifier que le service web tourne bien au niveau du systemd pour ce faire on fait la commande systemctl -t service -all | grep "web" pour voir son état au niveau de systemd.

```bash
[vagrant@test ~]$ systemctl -t service -all | grep "web"
  web.service                                           loaded    active   running Service Serveur Web Python2
```

Le résultat de la commande nous montre que le service web est bien entrain de tourné pour systemd.

On va aussi vérifier son status pour ce faire on fait la commande sudo systemctl status web.service -l

```bash
[vagrant@test ~]$ sudo systemctl status web.service -l
● web.service - Service Serveur Web Python2
   Loaded: loaded (/usr/lib/systemd/system/web.service; static; vendor preset: disabled)
   Active: active (running) since Wed 2020-10-07 09:43:35 UTC; 17min ago
  Process: 27532 ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${PORT}/tcp (code=exited, status=0/SUCCESS)
 Main PID: 27537 (python2)
   CGroup: /system.slice/web.service
           └─27537 /usr/bin/python2 -m SimpleHTTPServer 2402

Oct 07 09:43:34 test systemd[1]: Stopped Service Serveur Web Python2.
Oct 07 09:43:34 test systemd[1]: Starting Service Serveur Web Python2...
```

Le résultat de la commande nous montre que le service tourne bien et qu'il n'a pas rencontré d'erreur.

On veut aussi vérifier si la machine hôte peut accèder au serveur web pour ce faire on fait la commande curl 192.168.2.11:2402 depuis la machine hôte.

```bash
PS C:\Users\Crenn Antoine> curl 192.168.2.11:2402


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
                    <title>Directory listing for /</title>
                    <body>
                    <h2>Directory listing for /</h2>
                    <hr>
                    <ul>
                    <li><a href="bin/">bin@</a>
                    <li><a href="boot/">b...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 764
                    Content-Type: text/html; charset=UTF-8
                    Date: Wed, 07 Oct 2020 09:55:45 GMT
                    Server: SimpleHTTP/0.6 Python/2.7.5

                    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Fi...
Forms             : {}
Headers           : {[Content-Length, 764], [Content-Type, text/html; charset=UTF-8], [Date, Wed, 07 Oct 2020 09:55:45
                    GMT], [Server, SimpleHTTP/0.6 Python/2.7.5]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=bin@; innerText=bin@; outerHTML=<A href="bin/">bin@</A>; outerText=bin@; tagName=A;
                    href=bin/}, @{innerHTML=boot/; innerText=boot/; outerHTML=<A href="boot/">boot/</A>;
                    outerText=boot/; tagName=A; href=boot/}, @{innerHTML=dev/; innerText=dev/; outerHTML=<A
                    href="dev/">dev/</A>; outerText=dev/; tagName=A; href=dev/}, @{innerHTML=etc/; innerText=etc/;
                    outerHTML=<A href="etc/">etc/</A>; outerText=etc/; tagName=A; href=etc/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 764
```

Le résultat de la commande nous montre que le service web est bien fonctionnel et que la machine hôte peut bien accèder au serveur web.

##### B. Sauvegarde

Maintenant on va mettre en place un service de backup qui sera lancer avec un utilisateur dédié, il utilisera un fichier PID, il aura une description, le script sera séparé en 3 une partie test, une partie pour la sauvegarde et une partie pour la rotation des sauvegardes et on va aussi faire un fichier .timer pour que le service se lance toutes les heures.

Pour ce faire on a ajouter au script qui créer le service web toute les commandes pour la mise en place du service backup à savoir la création de l'utilisateur. On va aussi remplir le fichier qui permet de lancer le service backup, lui donner les permissions pour qu'il puisse passé des commandes sudo, remplir le fichier pour la partie de test pour la sauvegarde, remplir le fichier pour la partie de la sauvegarde, remplir le fichier pour la partie rotation pour la sauvegarde, remplir le fichier timer. Il faut créer le dossier de sauvegarde et changer le propriétaire du dossier pour que ça soit l'utilisateur dédié au service qui possède le dossier, lancer le timer et lancer le timer au boot de la machine. 

La commande qui permet d'afficher le contenu du script qui permet d'exécuter toutes les commandes pour la mise en place du service web et du service backup.


```bash
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat script_service.sh
#!/bin/bash
# Antoine Crenn
# 09/10/2020
# Permet de mettre en place le service web et le service de backup.

# Permet de définir l'adresse du serveur DNS que l'on veut utiliser.
sudo echo "nameserver 1.1.1.1" > /etc/resolv.conf
# Créer l'utilisateur web qui n'a pas de home et qui ne peut pas se connecter.
sudo useradd web -M -s /sbin/nologin;
# Créer l'utilisateur BACKUP qui a un home et qui ne peut pas se connecter.
sudo useradd backup -m -s /sbin/nologin;

# Rempli le fichier qui va permettre de créer le service web.

sudo echo "[Unit]
# Permet de rédiger la description du service web.
Description=Service Serveur Web Python2

[Service]
# Permet de scpécifier quel utilisateur lance le service.
User=web
# Mets le port en variable d'environnement.
Environment=\"PORT=2402\"
# Permet d'ouvrir le port avant de lancer le serveur web.
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=\${PORT}/tcp
# Lance le serveur web.
ExecStart=/usr/bin/python2 -m SimpleHTTPServer \${PORT}
# Permet de fermer le port après la fermeture du serveur web.
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=\${PORT}/tcp

[Install]
# Permet d'indiquer que le service a la possibilité d'être lancé au démarrage de la machine.
WantedBy=multi-user.target" > /etc/systemd/system/web.service

# Permet d'ajouter les droits sudo à l'utilisateur indiquer sans
# qu'il est besoin de rentrer de mot de passe.

sudo echo "web     ALL=(ALL)       NOPASSWD: ALL
backup     ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Rempli le fichier qui va permettre de créer le service backup.

sudo echo "[Unit]
# Permet de rédiger la description du service backup.
Description=Service de Backup pour site

[Service]
# Permet de scpécifier quel utilisateur lance le service.
User=backup
# Permet d'indiquer quel est le PIDfile du service.
PIDFile=/opt/backup/backup.pid
# Permet de lancer le script qui permet de faire les tests avant la sauvegarde.
ExecStartPre=/bin/bash /opt/test_sauvegarde.sh /srv
# Permet de lancer le script de sauvegarde.
ExecStart=/bin/bash /opt/sauvegarde.sh /srv
# Permet de lancer le script qui supprimer la sauvegarde la plus ancienne
# au bout d'un certain nombre.
ExecStartPost=/bin/bash /opt/nombre_sauvegarde.sh /srv" > /etc/systemd/system/backup.service

# Permet de remplir la partie du script de backup qui fait les tests.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script Test pour la Sauvegarde
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
chemin_fermer=\"/opt/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/opt/backup/\";

if test -z \${chemin_site};
then
        echo \"Vous avez oublié de rentrer l'argument.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${chemin_site};
then
        echo \"Le chemin du site \${chemin_site} n'existe pas.\" >&2;
        exit 1;
fi

if test -f \${chemin_fermer};
then
        exit 22;
fi

touch \${chemin_fermer};" > /opt/test_sauvegarde.sh

# Permet de remplir la partie du script de backup qui fait la sauvegarde
# et remplir le numéro du processus dans le PIDFile.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script de Sauvegarde
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
nom_sauvegarde=\"\${nom_site}_\${jour}_\${heure}\";
chemin_sauvegarde=\"/opt/backup/\";

echo \$\$ | sudo tee /opt/backup/backup.pid

tar Pzcvf \${chemin_sauvegarde}\${nom_sauvegarde} \${chemin_site} --ignore-failed-read > /dev/null;

if test \$? -eq 0;
then
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} c'est bien passé.\";
else
        echo \"La sauvegarde \${chemin_sauvegarde}\${nom_sauvegarde} a échoué.\" >&2;
        exit 1;
fi" > /opt/sauvegarde.sh

# Permet de remplir la partie du script de backup qui supprimer la dernière sauvegarde
# s'il y en a plus de 7 du même dossier.

sudo echo "#!/bin/bash
# Antoine Crenn
# 06/10/2020
# Script vérification du nombre de Sauvegarde
chemin_site=\"\${1}\";
nom_site=\${chemin_site##*/};
chemin_fermer=\"/opt/backup/lock_\${nom_site}\";
chemin_sauvegarde=\"/opt/backup/\";

nombre_sauvegarde=\$(ls \${chemin_sauvegarde} | grep -c \${nom_site});

if test \${nombre_sauvegarde} -ge 9;
then
        supprimer=\$(ls \${chemin_sauvegarde} | grep \${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf \${chemin_sauvegarde}\${supprimer};
fi

rm -rf \${chemin_fermer};" > /opt/nombre_sauvegarde.sh

# Créer un dossier backup dans le dossier opt, c'est l'endroit
# où l'on va enregistrer les sauvegarde faites par le service backup.

sudo mkdir /opt/backup

# Permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier backup
# pour que ce soit l'utilisateur backup et le groupe backup qui soient propriétaire.

sudo chown backup:backup /opt/backup

# Rempli le fichier backup.timer qui permet que le service backup

sudo echo "[Unit]
# Permet de rédiger la description du timer backup.
Description=Sauvegarde toutes les heures grace au service backup

[Timer]
# Indique que l'on veut que la répétion du timer soit toutes les heures.
OnCalendar=hourly
# Permet de lancer le service si la dernière exécution a été manquer.
Persistent=true

[Install]
# Permet d'indiquer que le timer a la possibilité d'être lancé au démarrage de la machine.
WantedBy=timers.target" > /etc/systemd/system/backup.timer

# Indique au système qu'il faut relire les fichiers ce qui permet qu'il trouve
# le ou les services que l'on a ajouté.

sudo systemctl daemon-reload

# Permet que le service web soit lancé au boot de la machine.

sudo systemctl enable web.service

# Permet de lancer le timer pour le service backup.

sudo systemctl start backup.timer

# Permet de lancer la minuterie pour le service backup au démarrage de la machine.

sudo systemctl enable backup.timer
```

[Script qui créer les services](./script/script_service.sh)

### II. Autres fonctionnalités

Pour cette partie nous allons utilisé la box centos8.

La commande qui permet d'afficher le contenu du fichier Vagrantfile qui utilise la VM centos8.

```ruby
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> cat Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.box = "centos/8"
  config.vbguest.auto_update = false
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "file", source: "C:\\Users\\Crenn Antoine\\.ssh\\id_rsa.pub", destination: "/home/vagrant/.ssh/authorized_keys"
  config.vm.define "test" do |test_debut|
    test_debut.vm.hostname = "test"
    test_debut.vm.network "private_network", ip: "192.168.2.11", netmask:"255.255.255.0"
    test_debut.vm.provider "virtualbox" do |test|
      test.name = "test"
      test.memory = "1024"
    end
  end
end
```

[Vagrantfile de la Partie 2](./Vagrantfile_partie2)

#### 1. Gestion de boot

On va utiliser systemd-analyze plot pour récuperer un diagramme de boot pour pouvoir voir les 3 services les plus lents à démarrer.

Pour ce faire on va faire la commande systemd-analyze plot que l'on va rediriger vers un fichier boot.svg.

```bash
[vagrant@test ~]$ systemd-analyze plot > boot.svg
```

Puis on va transferer le fichier boot.svg sur la machine hôte pour ce faire il faut ouvrir un port avec la commande sudo firewall-cmd --add-port=2402/tcp puis on peut utiliser la commande python2 -m SimpleHTTPServer 2402 puis sur l'hôte on va sur un navigateur on tappe l'adresse IP de la VM:le port là c'est 192.168.2.11:2402 et enfin on clique sur le lien boot.svg

La commande pour ouvrir le port 2402 au niveau du firewall jusqu'au redémarrage de la VM.

```bash
[vagrant@test ~]$ sudo firewall-cmd --add-port=2402/tcp
success
```

Le résultat de la commande nous montre que l'ajout du port c'est bien passé.

La commande pour lancé le serveur web grâce à Python sur le port 2402.

```bash
[vagrant@test ~]$ python2 -m SimpleHTTPServer 2402
Serving HTTP on 0.0.0.0 port 2402 ...
192.168.2.1 - - [09/Oct/2020 15:42:46] "GET / HTTP/1.1" 200 -
192.168.2.1 - - [09/Oct/2020 15:42:49] "GET /boot.svg HTTP/1.1" 200 -
^CTraceback (most recent call last):
  File "/usr/lib64/python2.7/runpy.py", line 174, in _run_module_as_main
    "__main__", fname, loader, pkg_name)
  File "/usr/lib64/python2.7/runpy.py", line 72, in _run_code
    exec code in run_globals
  File "/usr/lib64/python2.7/SimpleHTTPServer.py", line 235, in <module>
    test()
  File "/usr/lib64/python2.7/SimpleHTTPServer.py", line 231, in test
    BaseHTTPServer.test(HandlerClass, ServerClass)
  File "/usr/lib64/python2.7/BaseHTTPServer.py", line 610, in test
    httpd.serve_forever()
  File "/usr/lib64/python2.7/SocketServer.py", line 231, in serve_forever
    poll_interval)
  File "/usr/lib64/python2.7/SocketServer.py", line 150, in _eintr_retry
    return func(*args)
KeyboardInterrupt
```

Donc maintenant que l'on a accès au fichier boot.svg sur un navigateur on peut identifier les 3 services qui sont les plus lents au démarrage on voit que le plus lent est tuned.service le deuxième est sssd.service et que le troisième est sshd-keygen@rsa.service.

[Le fichier qui affiche la chaine de boot](./boot.svg)


#### 2. Gestion de l'heure

On cherche à déterminer le fuseau horaire avec timedatectl, on veut aussi déterminer si on est synchronisé avec un serveur NTP toujours avec timedatectl et on va aussi utilisé timedatectl pour changer de fuseau horaire.

La commande qui permet d'afficher le fuseau horaire et permet de voir si on est synchronisé avec un serveur NTP.

```bash
[vagrant@test ~]$ timedatectl
               Local time: Fri 2020-10-09 13:05:29 UTC
           Universal time: Fri 2020-10-09 13:05:29 UTC
                 RTC time: Fri 2020-10-09 13:05:29
                Time zone: UTC (UTC, +0000)
System clock synchronized: no
              NTP service: active
          RTC in local TZ: no
```

Le résultat de la commande nous montre que le fuseau horaire est Time zone: UTC (UTC, +0000) et que l'on est pas synchronisé avec un serveur NTP.

Maintenant on veut changer le fuseau horaire pour ce faire il faut d'abord trouvé un fuseau horaire on le fait avec la commande timedatectl list-timezones et après on fait la commande sudo timedatectl set-timezone suivi par le fuseau horaire choisi.

La commande pour afficher les fuseau horaire existant.

```bash
[vagrant@test ~]$ timedatectl list-timezones
[...]
```

La commande pour changé le fuseau horaire sur celui de Paris.

```bash
[vagrant@test ~]$ sudo timedatectl set-timezone Europe/Paris
```

Maintenant on vérifie que la modification a bien été prise en compte pour ce faire on refait la commande timedatectl.

```bash
[vagrant@test ~]$ timedatectl
               Local time: Fri 2020-10-09 15:34:54 CEST
           Universal time: Fri 2020-10-09 13:34:54 UTC
                 RTC time: Fri 2020-10-09 13:34:53
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: no
              NTP service: active
          RTC in local TZ: no
```

Le résultat de la commande nous montre que la modification a bien été prise en compte car on a la ligne Time zone: Europe/Paris (CEST, +0200).

#### 3. Gestion des noms et de la résolution de noms

On cherche à determiner le nom de la VM en utilisant hostnamecrl et à le changer en utilisant hostnamectl.

La commande pour afficher le nom de la VM.

```bash
[vagrant@test ~]$ hostnamectl
   Static hostname: test
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 6074ac0656124d938ec739cb428cf9b6
           Boot ID: e4f6389383bd4604ba9cbe049b3ba557
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

Le résultat de la commande nous montre que le nom de la VM est test.

Maintenant on va changé le nom de la VM avec la commande sudo hostnamectl set-hostname suivi du nom que l'on veut mettre à la VM.

La commande pour changer le nom de la VM test en salut.

```bash
[vagrant@test ~]$ sudo hostnamectl set-hostname salut
```

Maintenant on veut vérifier que la modification a bien été prise en compte pour ce faire on refait la commande hostnamectl.

```bash
[vagrant@test ~]$ hostnamectl
   Static hostname: salut
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 6074ac0656124d938ec739cb428cf9b6
           Boot ID: e4f6389383bd4604ba9cbe049b3ba557
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

Le résultat de la commande nous montre que la modification a bien été prise en compte car on a la ligne Static hostname: salut.