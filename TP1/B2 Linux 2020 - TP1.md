# B2 Linux 2020 - TP1

## TP1 : Déploiement classique

### 0. Prérequis

Nous allons faire la configuration basique des deux VMs Centos 7 que l'on va utilisé pendant le TP. Pour cela nous allons fournir un accès internet à chaque VM, un accès à un réseau local. On va aussi leur donner un nom, on va aussi faire qu'elles puisent ce ping avec leur nom respectif, on va aussi créer un utilisateur administrateur sur les deux VMs. On va utiliser ssh pour administrer les VMs, on va bloquer toutes les connexions exceptées celles qui sont nécessaires, on va désactiver SELinux et on va ajouter un deuxième disque de 5Go aux VMs et on va partitionner ce nouveau disque.

Pour vérifier que les VMs ont accès à Internet grâce à la carte NAT on fait un ping 8.8.8.8

Le ping pour vérifier que la VM node1 a accès à Internet.

```bash
[antoine@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=61.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=116 time=57.7 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 57.799/59.828/61.858/2.044 ms
```

Le résultat du ping montre que la VM node1 a bien accès à Internet.

Le ping pour vérifier que la VM node2 a accès à Internet.

```bash
[antoine@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=58.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=116 time=57.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=116 time=58.1 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 57.867/58.191/58.603/0.306 ms
```

Le résultat du ping montre que la VM node2 a bien accès à Internet.

Maintenant on va configurer l'accès au réseau local pour ce faire on va configurer le fichier /etc/sysconfig/network-scripts/ifcfg-enp0s8 avec la commande sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8 et vérifier que la configuration marche bien avec des ping.

La commande pour configurer l'interface host-only enp0s8 de la VM node1 et lui mettre l'adresse IP 192.168.1.11.

```bash
[antoine@node1 ~]$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8
```

La commande pour afficher le contenu du fichier /etc/sysconfig/network-scripts/ifcfg-enp0s8 de la VM node1.

```bash
[antoine@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
IPADDR=192.168.1.11
NETMASK=255.255.255.0
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
```

Le résultat de la commande nous montre la configuration de l'interface enp0s8 pour que la VM node1 est accès au réseau local et que son adresse IP est 192.168.1.11.

Maintenant on cherche à vérifier que la VM node1 arrive à ping l'hôte pour ce faire on fait un ping 192.168.1.1

```bash
[antoine@node1 ~]$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
64 bytes from 192.168.1.1: icmp_seq=1 ttl=128 time=0.516 ms
64 bytes from 192.168.1.1: icmp_seq=2 ttl=128 time=0.452 ms
64 bytes from 192.168.1.1: icmp_seq=3 ttl=128 time=0.331 ms
^C
--- 192.168.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 0.331/0.433/0.516/0.076 ms
```

Le résultat de la commande nous montre bien que la VM node1 peut bien contacter l'hôte.

La commande pour configurer l'interface host-only enp0s8 de la VM node2 et lui mettre l'adresse IP 192.168.1.12.

```bash
[antoine@node2 ~]$ sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8
```

La commande pour afficher le contenu du fichier /etc/sysconfig/network-scripts/ifcfg-enp0s8 de la VM node2.

```bash
[antoine@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=192.168.1.12
NETMASK=255.255.255.0
```

Le résultat de la commande nous montre la configuration de l'interface enp0s8 pour que la VM node2 est accès au réseau local et que son adresse IP est 192.168.1.12.

Maintenant on cherche à vérifier que la VM node2 arrive à ping l'hôte pour ce faire on fait un ping 192.168.1.1

```bash
[antoine@node2 ~]$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
64 bytes from 192.168.1.1: icmp_seq=1 ttl=128 time=0.590 ms
64 bytes from 192.168.1.1: icmp_seq=2 ttl=128 time=0.429 ms
64 bytes from 192.168.1.1: icmp_seq=3 ttl=128 time=0.446 ms
^C
--- 192.168.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 0.429/0.488/0.590/0.074 ms
```

Le résultat de la commande nous montre bien que la VM node2
peut bien contacter l'hôte.

Maintenant on cherche à vérifier si les deux VMs arrivent à se ping.

Le ping de la VM node2 depuis la VM node1 pour vérifier qu'elles peuvent se contacter.

```bash
[antoine@node1 ~]$ ping 192.168.1.12
PING 192.168.1.12 (192.168.1.12) 56(84) bytes of data.
64 bytes from 192.168.1.12: icmp_seq=1 ttl=64 time=0.616 ms
64 bytes from 192.168.1.12: icmp_seq=2 ttl=64 time=0.342 ms
^C
--- 192.168.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.342/0.479/0.616/0.137 ms
```

Le résultat du ping de la VM node2 depuis la VM node1 montre que la VM node1 et la VM node2 peuvent se contacter.

Pour être sur qu'elles peuvent se ping on va effectuer le ping dans l'autre sens donc on va ping la VM node1 depuis la VM node2.

```bash
[antoine@node2 ~]$ ping 192.168.1.11
PING 192.168.1.11 (192.168.1.11) 56(84) bytes of data.
64 bytes from 192.168.1.11: icmp_seq=1 ttl=64 time=0.539 ms
64 bytes from 192.168.1.11: icmp_seq=2 ttl=64 time=0.338 ms
64 bytes from 192.168.1.11: icmp_seq=3 ttl=64 time=0.352 ms
^C
--- 192.168.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2000ms
rtt min/avg/max/mdev = 0.338/0.409/0.539/0.094 ms
```

Le résultat du ping de la VM node1 depuis la VM node2 nous confirme que la VM node1 et la VM node2 peuvent se contacter.

Maintenant on va mettre leur nom respectif à chaque VM pour ce faire on va faire la commande sudo vi /etc/hostname et on fera la commande cat /etc/hostname pour afficher le contenu du fichier /etc/hostname.

La commande pour écrire le nom de la VM node1 dans le fichier associé à la gestion du nom de la VM.

```bash
[antoine@node1 ~]$ sudo vi /etc/hostname
```

La commande pour afficher le contenu du fichier /etc/hostname qui est associé à la gestion du nom de la VM.

```bash
[antoine@node1 ~]$ cat /etc/hostname
node1
```

La commande pour écrire le nom de la VM node2 dans le fichier associé à la gestion du nom de la VM.

```bash
[antoine@node2 ~]$ sudo vi /etc/hostname
```

La commande pour afficher le contenu du fichier /etc/hostname qui est associé à la gestion du nom de la VM.

```bash
[antoine@node2 ~]$ cat /etc/hostname
node2
```

Maintenant on va faire que les deux VMs puissent se ping avec leurs noms respectifs pour ce faire on va associé leur adresse IP à leur nom dans le fichier /etc/hosts avec la commande sudo vi /etc/hosts et on fera la commande cat /etc/hosts pour afficher le contenu du fichier /etc/hosts.

La commande pour pourvoir écrire l'adresse IP de la VM node2 à son nom dans le fichier /etc/hosts qui permer d'associer une adresse IP à un nom.

```bash
[antoine@node1 ~]$ sudo vi /etc/hosts
```

La commande pour afficher le contenu du fichier /etc/hosts qui associe l'adresse IP à un nom.

```bash
[antoine@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.12 node2
```

La commande pour pourvoir écrire l'adresse IP de la VM node1 à son nom dans le fichier /etc/hosts qui permer d'associer une adresse IP à un nom.

```bash
[antoine@node2 ~]$ sudo vi /etc/hosts
```

La commande pour afficher le contenu du fichier /etc/hosts qui associe l'adresse IP à un nom.

```bash
[antoine@node2 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.11 node1
```

Pour vérifier que la configuration faites dans le fichier /etc/hosts sur chaque VM marche on va faire un ping avec le nom de l'autre VM à la place de l'adresse IP.

Le ping de la VM node2 depuis la VM node1 avec le nom de la VM node2.

```bash
[antoine@node1 ~]$ ping node2
PING node2 (192.168.1.12) 56(84) bytes of data.
64 bytes from node2 (192.168.1.12): icmp_seq=1 ttl=64 time=0.328 ms
64 bytes from node2 (192.168.1.12): icmp_seq=2 ttl=64 time=0.415 ms
^C
--- node2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.328/0.371/0.415/0.047 ms
```

Le résultat du ping de la VM node2 depuis la VM node1 nous confirme que la configuration que l'on a faite pour associé l'adresse IP de la VM node2 à son nom marche.

Le ping de la VM node1 depuis la VM node2 avec le nom de la VM node1.

```bash
[antoine@node2 ~]$ ping node1
PING node1 (192.168.1.11) 56(84) bytes of data.
64 bytes from node1 (192.168.1.11): icmp_seq=1 ttl=64 time=0.394 ms
64 bytes from node1 (192.168.1.11): icmp_seq=2 ttl=64 time=0.349 ms
64 bytes from node1 (192.168.1.11): icmp_seq=3 ttl=64 time=0.420 ms
^C
--- node1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2009ms
rtt min/avg/max/mdev = 0.349/0.387/0.420/0.037 ms
```

Le résultat du ping de la VM node1 depuis la VM node2 nous confirme que la configuration que l'on a faite pour associé l'adresse IP de la VM node1 à son nom marche.

Maintenant on va s'occuper de la partie SSH sur les deux VMs pour ce faire on va créer le dossier .ssh avec la commande mkdir .ssh et on va copier la clé publique de l'hôte sur chaque VM.

La commande pour créer le dossier .ssh sur la VM node1.

```bash
[antoine@node1 ~]$ mkdir .ssh
```

La commande pour copier la clé publique qui se situe dans le fichier id_rsa.pub qui est dans le dossier .ssh sur l'hôte vers le fichier authorized_keys dans le dossier .ssh sur la VM node1.

```bash
PS C:\Users\Crenn Antoine> scp .ssh\id_rsa.pub antoine@192.168.1.11:.ssh/authorized_keys
antoine@192.168.1.11's password:
id_rsa.pub                                                                            100%  580   295.8KB/s   00:00
```

Le résultat de la commande nous montre que la clé publique de l'hôte a bien été copier sur la VM node1.

La commande pour créer le dossier .ssh sur la VM node2.

```bash
[antoine@node2 ~]$ mkdir .ssh
```

La commande pour copier la clé publique qui se situe dans le fichier id_rsa.pub qui est dans le dossier .ssh sur l'hôte vers le fichier authorized_keys dans le dossier .ssh sur la VM node2.

```bash
PS C:\Users\Crenn Antoine> scp .ssh\id_rsa.pub antoine@192.168.1.12:.ssh/authorized_keys
antoine@192.168.1.12's password:
id_rsa.pub                                                                            100%  580   290.2KB/s   00:00
```

Le résultat de la commande nous montre que la clé publique de l'hôte a bien été copier sur la VM node2.

Maintenant on va s'occuper des autorisations du dossier .ssh et sur le fichier authorized_keys pour que la connection par clé fonctionne pour ce faire on va utilisé la commande chmod.

La commande chmod qui permet de donner les autorisations de lecture, d'écriture et d'éxécution à l'utilisateur sur le dossier .ssh.


```bash
[antoine@node1 ~]$ chmod 700 .ssh
```

La commande chmod qui permet de donner les autorisations de lecture et d'écriture à l'utilisateur sur le fichier authorized_keys.

```bash
[antoine@node1 ~]$ chmod 600 .ssh/authorized_keys
```

La commande chmod qui permet de donner les autorisations de lecture, d'écriture et d'éxécution à l'utilisateur sur le dossier .ssh.

```bash
[antoine@node2 ~]$ chmod 700 .ssh
```

La commande chmod qui permet de donner les autorisations de lecture et d'écriture à l'utilisateur sur le fichier authorized_keys.

```bash
[antoine@node2 ~]$ chmod 600 .ssh/authorized_keys
```

Maintenant on va s'occuper du partitionnement pour se faire sur VirtualBox on a ajouté un disque de 5Go au deux VMs. On veut faire deux partitions la première partition de 2Go qui sera montée sur /srv/site1 et la deuxième de 3Go qui sera montée sur /srv/site2. On va utiliser LVM qui permet de partitionner de façon logique, pour commencer on va ajouter le disque en tant que volume physique on le fait avec la commande pvcreate.

La commande pvcreate avec le disque /dev/sdb qui est le disque de 5Go que l'on a ajouté pour l'ajouté en volume physique pour la VM node1.

```bash
[antoine@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for antoine:
  Physical volume "/dev/sdb" successfully created.
```

Le résultat du pvcreate nous montre que le disque de 5Go a bien été ajouter au volume physique de la VM node1.
  
La commande pvcreate avec le disque /dev/sdb qui est le disque de 5Go que l'on a ajouté pour l'ajouté en volume physique pour la VM node2. 
  
```bash
[antoine@node2 ~]$ sudo pvcreate /dev/sdb
[sudo] password for antoine:
  Physical volume "/dev/sdb" successfully created.
```

Le résultat du pvcreate nous montre que le disque de 5Go a bien été ajouter au volume physique de la VM node2.

Maintenant on va vérifier que l'ajout du disque de 5Go en volume physique a bien été pris en compte pour ce faire on va faire la commande pvdisplay sur les deux VMs.

La commande pvdisplay sur la VM node1 pour vérifier l'ajout du disque 5Go en volume physique.
  
```bash
[antoine@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               centos
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               YntBFp-43ez-zXek-SdHu-35T7-Ke3x-Y074gL

  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               3FKNJJ-Pogd-9X5i-FiRv-Cp7R-SgsH-RMDCdz
```

Le résultat de la commande pvdisplay nous montre que l'ajout a bien été pris en compte sur la VM node1 car on a le PV Name /dev/sdb qui est le nom de notre disque de 5Go.

La commande pvdisplay sur la VM node1 pour vérifier l'ajout du disque 5Go en volume physique.
  
```bash
[antoine@node2 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               centos
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               YntBFp-43ez-zXek-SdHu-35T7-Ke3x-Y074gL

  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               iSXuCo-vlFQ-jllc-rQUd-g9iH-prQS-COK86R
```

Le résultat de la commande pvdisplay nous montre que l'ajout a bien été pris en compte sur la VM node2 car on a le PV Name /dev/sdb qui est le nom de notre disque de 5Go.

Maintenant on va créer un volume groupe que l'on va appelle donnee où l'on va mettre notre volume physique que l'on a créer précédemment, on va le faire avec la commande vgcreate.

La commande vgcreate qui est suvi par le nom du volume groupe que l'on veut créer ici c'est donnee et après on écrit le nom du disque que l'on va ajouté au volume groupe ici /dev/sdb.
  
```bash
[antoine@node1 ~]$ sudo vgcreate donnee /dev/sdb
  Volume group "donnee" successfully created
```

Le résultat nous montre que la création du volume groupe donnee c'est bien passer sur la VM node1.
 
La commande vgcreate qui est suvi par le nom du volume groupe que l'on veut créer ici c'est donnee et après on écrit le nom du disque que l'on va ajouté au volume groupe ici /dev/sdb. 
  
```bash
[antoine@node2 ~]$ sudo vgcreate donnee /dev/sdb
  Volume group "donnee" successfully created
```

Le résultat nous montre que la création du volume groupe donnee c'est bien passer sur la VM node1.

Maintenant on va vérifier que la création du volume groupe donnee a  bien été pris en compte pour ce faire on va faire la commande vgdisplay sur les deux VMs.

La commande vgdisplay sur la VM node1 pour vérifier la création de donnee.

```bash
[antoine@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               donnee
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               2cWZ9Z-H32o-B80A-jIk0-Q9TD-uUNT-HgODcn

  --- Volume group ---
  VG Name               centos
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <7.00 GiB
  PE Size               4.00 MiB
  Total PE              1791
  Alloc PE / Size       1791 / <7.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               kEsAXP-CiSC-IYbk-3fIN-q3oD-OLow-dC1QBp  
```

Le résultat du vgdisplay nous montre que la création du volume groupe donnee a été prise en compte sur la VM node1 car on a bien le volume groupe donnee.
  
La commande vgdisplay sur la VM node1 pour vérifier la création de donnee.
  
```bash
[antoine@node2 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               donnee
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <5.00 GiB
  PE Size               4.00 MiB
  Total PE              1279
  Alloc PE / Size       0 / 0
  Free  PE / Size       1279 / <5.00 GiB
  VG UUID               HK9n3n-7DHA-OC2a-jG7J-jnYx-rqof-WaiBxw

  --- Volume group ---
  VG Name               centos
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <7.00 GiB
  PE Size               4.00 MiB
  Total PE              1791
  Alloc PE / Size       1791 / <7.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               kEsAXP-CiSC-IYbk-3fIN-q3oD-OLow-dC1QBp
```
  
Le résultat du vgdisplay nous montre que la création du volume groupe donnee a été prise en compte sur la VM node2 car on a bien le volume groupe donnee. 

Maintenant on va créer les deux volumes logiques sur les deux VMs pour ce faire on va utiliser la commande lvcreate.

La commande lvcreate sur la VM node1 pour créer le premier volume logique le tiret grand L c'est pour spécifier l taille du volume logique qui là doit faire 2Go et le tiret n c'est pour spécifier le nom du volume logique qui là est LV_srv.

```bash
[antoine@node1 ~]$ sudo lvcreate -L 2G donnee -n LV_srv
  Logical volume "LV_srv" created.
```

Le résultat du lvcreate nous montre qu'il y a bien eu la création du volume logique LV_srv.
  
La commande lvcreate sur la VM node1 pour créer le deuxième volume logique le tiret grand l 100%FREE c'est pour spécifier qu'il prend tous l'espace libre restant qui là est de 3Go ce que l'on veut pour le deuxième volume logique et le tiret n c'est pour spécifier le nom du volume logique qui là est LV_home.

```bash
[antoine@node1 ~]$ sudo lvcreate -l 100%FREE donnee -n LV_home
  Logical volume "LV_home" created. 
```

Le résultat du lvcreate nous montre qu'il y a bien eu la création du volume logique LV_home.
  
La commande lvcreate sur la VM node2 pour créer le premier volume logique le tiret grand L c'est pour spécifier la taille du volume logique qui la doit faire 2Go et le tiret n c'est pour spécifier le nom du volume logique qui là est LV_srv.
  
```bash
[antoine@node2 ~]$ sudo lvcreate -L 2G donnee -n LV_srv
  Logical volume "LV_srv" created.
```

Le résultat du lvcreate nous montre qu'il y a bien eu la création du volume logique LV_srv.

La commande lvcreate sur la VM node2 pour créer le deuxième volume logique le tiret grand l 100%FREE c'est pour spécifier qu'il prend tous l'espace libre restant qui là est de 3Go ce que l'on veut pour le deuxième volume logique et le tiret n c'est pour spécifier le nom du volume logique qui là est LV_home.

```bash
[antoine@node2 ~]$ sudo lvcreate -l 100%FREE donnee -n LV_home
  Logical volume "LV_home" created.
```

Le résultat du lvcreate nous montre qu'il y a bien eu la création du volume logique LV_home.

Maintenant on va vérifier qu'il y a bien eu la création des deux volumes logiques et qu'ils ont la bonne taille pour ce faire on va faire la commande lvdisplay sur les deux VMs.
  
La commande lvdisplay sur la VM node1 pour vérifier la présence des deux volumes logiques que l'on a créer et vérifier leurs tailles.

```bash
[antoine@node1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/donnee/LV_srv
  LV Name                LV_srv
  VG Name                donnee
  LV UUID                lLPzxy-UOfd-x8An-aufR-fWda-VL0e-uNztrM
  LV Write Access        read/write
  LV Creation host, time node1, 2020-09-23 12:55:46 +0200
  LV Status              available
  # open                 0
  LV Size                2.00 GiB
  Current LE             512
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/donnee/LV_home
  LV Name                LV_home
  VG Name                donnee
  LV UUID                k1zPOX-8jPE-lMUk-1iRz-UCZy-if1f-Iqzser
  LV Write Access        read/write
  LV Creation host, time node1, 2020-09-23 12:56:02 +0200
  LV Status              available
  # open                 0
  LV Size                <3.00 GiB
  Current LE             767
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/centos/swap
  LV Name                swap
  VG Name                centos
  LV UUID                wuOoms-UIw8-Kn02-qD0X-U5Kk-Rx3r-KdxGnP
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-09-23 09:50:23 +0200
  LV Status              available
  # open                 2
  LV Size                820.00 MiB
  Current LE             205
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/centos/root
  LV Name                root
  VG Name                centos
  LV UUID                XwFYfb-YZi1-bZee-2NlC-ZocR-Apd6-K3WvwV
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-09-23 09:50:23 +0200
  LV Status              available
  # open                 1
  LV Size                <6.20 GiB
  Current LE             1586
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```

Le résultat de la commande nous montre que la création des deux volumes logiques a bien été faite et qu'ils ont bien le volume voulu qui est de 2Go pour le premier et de 3Go pour le second.
  
La commande lvdisplay sur la VM node2 pour vérifier la présence des deux volumes logiques que l'on a créer et vérifier leurs tailles. 

```bash
[antoine@node2 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/donnee/LV_srv
  LV Name                LV_srv
  VG Name                donnee
  LV UUID                2QaLfh-ZJd3-gd0y-1CS2-29EV-fygB-fL5wMZ
  LV Write Access        read/write
  LV Creation host, time node2, 2020-09-23 12:56:47 +0200
  LV Status              available
  # open                 0
  LV Size                2.00 GiB
  Current LE             512
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/donnee/LV_home
  LV Name                LV_home
  VG Name                donnee
  LV UUID                dhS8JK-H4iU-qzOS-LiFg-S83n-haSj-NXz5w9
  LV Write Access        read/write
  LV Creation host, time node2, 2020-09-23 12:56:55 +0200
  LV Status              available
  # open                 0
  LV Size                <3.00 GiB
  Current LE             767
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/centos/swap
  LV Name                swap
  VG Name                centos
  LV UUID                wuOoms-UIw8-Kn02-qD0X-U5Kk-Rx3r-KdxGnP
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-09-23 09:50:23 +0200
  LV Status              available
  # open                 2
  LV Size                820.00 MiB
  Current LE             205
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/centos/root
  LV Name                root
  VG Name                centos
  LV UUID                XwFYfb-YZi1-bZee-2NlC-ZocR-Apd6-K3WvwV
  LV Write Access        read/write
  LV Creation host, time localhost, 2020-09-23 09:50:23 +0200
  LV Status              available
  # open                 1
  LV Size                <6.20 GiB
  Current LE             1586
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```

Le résultat de la commande nous montre que la création des deux volumes logiques a bien été faite et qu'ils ont bien le volume voulu qui est de 2Go pour le premier et de 3Go pour le second.

Maintenant nous allons formater les partitions en ext4 avec la commande mkfs.

La commande mkfs de la partition LV_srv de la VM node1 pour la formaté en ext4.
  
```bash
[antoine@node1 ~]$ sudo mkfs -t ext4 /dev/donnee/LV_srv
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Le résultat de la commande mkfs nous montres que la partition LV_srv de la VM node1 a été paritionner.

La commande mkfs de la partition LV_home de la VM node1 pour la formaté en ext4.

```bash
[antoine@node1 ~]$ sudo mkfs -t ext4 /dev/donnee/LV_home
[sudo] password for antoine:
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
196608 inodes, 785408 blocks
39270 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=805306368
24 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Le résultat de la commande mkfs nous montres que la partition LV_home de la VM node1 a été paritionner.

La commande mkfs de la partition LV_srv de la VM node2 pour la formaté en ext4.

```bash
[antoine@node2 ~]$ sudo mkfs -t ext4 /dev/donnee/LV_srv
[sudo] password for antoine:
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Le résultat de la commande mkfs nous montres que la partition LV_srv de la VM node2 a été paritionner.

La commande mkfs de la partition LV_home de la VM node2 pour la formaté en ext4.

```bash
[antoine@node2 ~]$ sudo mkfs -t ext4 /dev/donnee/LV_home
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
196608 inodes, 785408 blocks
39270 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=805306368
24 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Le résultat de la commande mkfs nous montres que la partition LV_home de la VM node2 a été paritionner.

Maintenant nous allons créer les dossiers où nous allons monter les paritions pour ce faire on va utiliser la commande mkdir et la commande mount.

La commande mkdir pour créer le point de montage /srv/site1 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo mkdir /srv/site1
```

La commande mkdir pour créer le point de montage /srv/site2 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo mkdir /srv/site2
```

La commande pour monter la partition /dev/donnee/LV_srv sur le point de montage /srv/site1 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo mount /dev/donnee/LV_srv /srv/site1 
```

La commande pour monter la partition /dev/donnee/LV_home sur le point de montage /srv/site2 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo mount /dev/donnee/LV_home /srv/site2
```

La commande mkdir pour créer le point de montage /srv/site1 sur la VM node2.

```bash
[antoine@node2 ~]$ sudo mkdir /srv/site1
```

La commande mkdir pour créer le point de montage /srv/site2 sur la VM node2.

```bash
[antoine@node2 ~]$ sudo mkdir /srv/site2
```

La commande pour monter la partition /dev/donnee/LV_srv sur le point de montage /srv/site1 sur la VM node2.

```bash
[antoine@node2 ~]$ sudo mount /dev/donnee/LV_srv /srv/site1
```

La commande pour monter la partition /dev/donnee/LV_home sur le point de montage /srv/site2 sur la VM node2.

```bash
[antoine@node2 ~]$ sudo mount /dev/donnee/LV_home /srv/site2
```

Maintenant on veut vérifier que l'on bien monté les deux paritions sur les bons points de montages et que l'on l'a bien fait sur les deux VMs.

La commande df -h sur la VM node 1 pour afficher l'usage du disque et la taille, nous permet de voir les différentes paritions.

```bash
[antoine@node1 ~]$ df -h
Filesystem                  Size  Used Avail Use% Mounted on
devtmpfs                    484M     0  484M   0% /dev
tmpfs                       496M     0  496M   0% /dev/shm
tmpfs                       496M  6.8M  489M   2% /run
tmpfs                       496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root     6.2G  1.8G  4.5G  28% /
/dev/sda1                  1014M  167M  848M  17% /boot
/dev/mapper/donnee-LV_srv   2.0G  6.0M  1.8G   1% /srv/site1
tmpfs                       100M     0  100M   0% /run/user/1000
/dev/mapper/donnee-LV_home  2.9G  9.0M  2.8G   1% /srv/site2
```

Le résultat de la commande df -h nous montre que l'on a bien monté les deux partitions sur leur point de montage respectif car on voit que l'on a /dev/mapper/donnee-LV_srv qui est monté sur /srv/site1 et qu'elle fait 2Go et on voit que l'on a /dev/mapper/donnee-LV_home qui est monté sur /srv/site2 et qu'elle fait environ 3Go.

La commande df -h sur la VM node 1 pour afficher l'usage du disque et la taille, nous permet de voir les différentes paritions.

```bash
[antoine@node2 ~]$ df -h
Filesystem                  Size  Used Avail Use% Mounted on
devtmpfs                    484M     0  484M   0% /dev
tmpfs                       496M     0  496M   0% /dev/shm
tmpfs                       496M  6.8M  489M   2% /run
tmpfs                       496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root     6.2G  1.4G  4.8G  23% /
/dev/sda1                  1014M  167M  848M  17% /boot
tmpfs                       100M     0  100M   0% /run/user/1000
/dev/mapper/donnee-LV_srv   2.0G  6.0M  1.8G   1% /srv/site1
/dev/mapper/donnee-LV_home  2.9G  9.0M  2.8G   1% /srv/site2
```

Le résultat de la commande df -h nous montre que l'on a bien monté les deux partitions sur leur point de montage respectif car on voit que l'on a /dev/mapper/donnee-LV_srv qui est monté sur /srv/site1 et qu'elle fait 2Go et on voit que l'on a /dev/mapper/donnee-LV_home qui est monté sur /srv/site2 et qu'elle fait environ 3Go.

Maintenant on veut que les partitions se montent automatiquement au démarrage de la VM pour ce faire on va l'inscrire dans le fichier /etc/fstab avec la commande sudo vi /etc/fstab

La commande pour pouvoir acceder au fichier qui permet d'associé une partition à un point de montage pour qu'elle soit monté au démarrage de la VM node1.

```bash
[antoine@node1 ~]$ sudo vi /etc/fstab
```

La commande qui permet d'afficher la configuration faite dans le fichier /etc/fstab pour que la partition /dev/mapper/donnee-LV_srv soit monté sur /srv/site1 au démarrage de la VM node1 et que la partition /dev/mapper/donnee-LV_home soit monté sur /srv/site2 au démarrage de la VM node1.

```bash
[antoine@node1 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 23 09:50:24 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=fc0425c7-c103-4349-bf4c-bcce35ed05da /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/mapper/donnee-LV_srv   /srv/site1       ext4      defaults        0 0
/dev/mapper/donnee-LV_home  /srv/site2       ext4      defaults        0 0
```

La commande pour pouvoir acceder au fichier qui permet d'associé une partition à un point de montage pour qu'elle soit monté au démarrage de la VM node2.

```bash
[antoine@node2 ~]$ sudo vi /etc/fstab
```

La commande qui permet d'afficher la configuration faite dans le fichier /etc/fstab pour que la partition /dev/mapper/donnee-LV_srv soit monté sur /srv/site1 au démarrage de la VM node2 et que la partition /dev/mapper/donnee-LV_home soit monté sur /srv/site2 au démarrage de la VM node2.

```bash
[antoine@node2 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 23 09:50:24 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=fc0425c7-c103-4349-bf4c-bcce35ed05da /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/mapper/donnee-LV_srv   /srv/site1       ext4      defaults        0 0
/dev/mapper/donnee-LV_home  /srv/site2       ext4      defaults        0 0
```

Maintenant on veut vérifier que la configuration que l'on a fait dans le fichier /etc/fstab marche bien pour ce faire on va démonter les deux partitions avec la commande umount et on les montera g^race au fichier avec la commande mount -av.

La commande pour démonté la partition monté sur le point de montage /srv/site1 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo umount /srv/site1
```

La commande pour démonté la partition monté sur le point de montage /srv/site2 sur la VM node1.

```bash
[antoine@node1 ~]$ sudo umount /srv/site2
```

La commande pour monter les deux partions sur leur point de montage respectif grâce au fichier /etc/fstab.

```bash
[antoine@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
swap                     : ignored
mount: /srv/site1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site1               : successfully mounted
mount: /srv/site2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site2               : successfully mounted
```

Le résultat de la commande nous montre que les partitions ont bien été monter sur leur point de montage respectif.

La commande pour démonté la partition monté sur le point de montage /srv/site1 sur la VM node1.

```bash
[antoine@node2 ~]$ sudo umount /srv/site1
```

La commande pour démonté la partition monté sur le point de montage /srv/site2 sur la VM node2.

```bash
[antoine@node2 ~]$ sudo umount /srv/site2
```

La commande pour monter les deux partions sur leur point de montage respectif grâce au fichier /etc/fstab.

```bash
[antoine@node2 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
swap                     : ignored
mount: /srv/site1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site1               : successfully mounted
mount: /srv/site2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site2               : successfully mounted
```

Le résultat de la commande nous montre que les partitions ont bien été monter sur leur point de montage respectif.

Maintenant on veut créer un utilisateur administrateur qui peut exécuter des commandes sudo sur les deux VMs. Cet utilisaeur sera l'utilisateur admin.

La commande useradd pour créer l'utilisateur admin sur la VM node1.

```bash
[antoine@node1 ~]$ sudo useradd admin
```

La commande passwd pour donner un mot de passe à l'utilisateur admin sur la VM node1.

```bash
[antoine@node1 ~]$ sudo passwd admin
Changing password for user admin.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

La commande pour pouvoir rédiger les autorisations de l'utilisateur admin dans le fichier /etc/sudoers pour qu'il puisse lancer des commandes sudo.

```bash
[antoine@node1 ~]$ sudo visudo
```

Les commandes pour afficher les autorisations que l'on a mis à l'utilisateur admin.

```bash
[antoine@node2 ~]$ sudo cat /etc/sudoers | grep admin
admin   ALL=(ALL)       ALL
```

La commande useradd pour créer l'utilisateur admin sur la VM node2.

```bash
[antoine@node2 ~]$ sudo useradd admin
```

La commande passwd pour donner un mot de passe à l'utilisateur admin sur la VM node2.

```bash
[antoine@node2 ~]$ sudo passwd admin
Changing password for user admin.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

La commande pour pouvoir rédiger les autorisations de l'utilisateur admin dans le fichier /etc/sudoers pour qu'il puisse lancer des commandes sudo.


```bash
[antoine@node2 ~]$ sudo visudo
```

Les commandes pour afficher les autorisations que l'on a mis à l'utilisateur admin.

```bash
[antoine@node2 ~]$ sudo cat /etc/sudoers | grep admin
admin   ALL=(ALL)       ALL
```

Maintenant nous voulons configurer le firewall pour qu'il soit le moins permissif possible pour ce faire on va drop tous ce que l'on veut pas donc on va créer une zone que l'on va appeller drop qui ne laissera que les services que l'on aura autorisé.

La commande pour rédigé une zone customisé pour le firewall de la VM node1.

```bash
[antoine@node1 ~]$ sudo vi /etc/firewalld/zones/custom.xml
```

La commande pour afficher le contenu du fichier de configuration de la zone customisé du firewall on voit que le seul service autorisé c'est le ssh.

```bash
[antoine@node1 ~]$ sudo cat /etc/firewalld/zones/custom.xml
<?xml version="1.0" encoding="utf-8"?>
<zone target="DROP">
  <short>Zone Configuration Perso</short>
  <description>Toutes les connections sont bloqués. Il y a seulement les services précisé qui fonctionnent.</description>
  <service name="ssh"/>
</zone>
```

La commande qui permet d'autorisé les pings entrants au niveau du firewall pour ne pas bloquer les possibles échanegs entres les deux VMs.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p icmp -s 0.0.0.0/0 -d 0.0.0.0/0 -j ACCEPT
success
```

La commande pour relancer le firewall pour qu'il prend en compte les modifications que l'on a fait.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --reload
success
```

La commande pour afficher les services qui sont actifs au niveau du firewall et les ports qui sont ouverts.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

La commande pour rédigé une zone customisé pour le firewall de la VM node2.

```bash
[antoine@node2 ~]$ sudo vi /etc/firewalld/zones/custom.xml
```

La commande pour afficher le contenu du fichier de configuration de la zone customisé du firewall on voit que le seul service autorisé c'est le ssh.

```bash
[antoine@node2 ~]$ sudo cat /etc/firewalld/zones/custom.xml
<?xml version="1.0" encoding="utf-8"?>
<zone target="DROP">
  <short>Zone Configuration Perso</short>
  <description>Toutes les connections sont bloqués. Il y a seulement les services précisé qui fonctionnent.</description>
  <service name="ssh"/>
</zone>
```

La commande qui permet d'autorisé les pings entrants au niveau du firewall pour ne pas bloquer les possibles échanegs entres les deux VMs.

```bash
[antoine@node2 ~]$ sudo firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p icmp -s 0.0.0.0/0 -d 0.0.0.0/0 -j ACCEPT
success
```

La commande pour relancer le firewall pour qu'il prend en compte les modifications que l'on a fait.

```bash
[antoine@node2 ~]$ sudo firewall-cmd --reload
success
```

La commande pour afficher les services qui sont actifs au niveau du firewall et les ports qui sont ouverts.

```bash
[antoine@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Et enfin on va désactivé SELinux pour ce faire on fait la commande sudo setenforce 0 et on va remplacer enforcing par permissive dans le fichier /etc/selinux/config.

La commande pour désactiver temporairement SELinux pour la VM node1.

```bash
[antoine@node1 ~]$ sudo setenforce 0
```

La commande pour pouvoir remplacer enforcing par permissive dans le fichier /etc/selinux/config.

```bash
[antoine@node1 ~]$ sudo vi /etc/selinux/config
```

La commande pour afficher le contenu du fichier /etc/selinux/config.

```bash
[antoine@node1 ~]$ cat /etc/selinux/config

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

La commande pour désactiver temporairement SELinux pour la VM node2.

```bash
[antoine@node2 ~]$ sudo setenforce 0
```

La commande pour pouvoir remplacer enforcing par permissive dans le fichier /etc/selinux/config.

```bash
[antoine@node2 ~]$ sudo vi /etc/selinux/config
```

La commande pour afficher le contenu du fichier /etc/selinux/config.

```bash
[antoine@node2~]$ cat /etc/selinux/config

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

### I. Setup serveur Web

On va configurer deux sites web sur la VM node1 pour ce faire il faut installer epel-release et nginx et on va créer un utilisateur dédier au fonctionnement des deux sites.

La commande pour installer epel-release est la suivante.

```bash
[antoine@node1 ~]$ sudo yum install -y epel-release
```

La commande pour installer nginx est la suivante.

```bash
[antoine@node1 ~]$ sudo yum install -y nginx
```

On va créer un utilisateur dédier aux deux sites cet utilisateur sera l'utilisateur web pour ce faire on va faire la commande sudo useradd -m web.

```bash
[antoine@node1 ~]$ sudo useradd -m web
```

On va aussi mettre un mot de passe à l'utilisateur web pour ce faire on va fait la commande sudo passwd web.

```bash
[antoine@node1 ~]$ sudo passwd web
[sudo] password for antoine:
Changing password for user web.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

Maintenant on va s'occuper de remplir les fichiers index.html des deux sites et des autoriastions des deux sites. Pour ce faire on va faire la commande sudo vi /srv/site1/index.html pour remplir les fichiers index.html.

La commande pour écrire dans le fichier index.html du site 1.

```bash
[antoine@node1 ~]$ sudo vi /srv/site1/index.html
```

La commande pour afficher le contenu du fichier index.html du site 1.

```bash
[antoine@node1 ~]$ sudo cat /srv/site1/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>
```

La commande chmod qui permet de donner les autorisations de lecture et d'éxécution à l'utilisateur et au groupe sur le dossier site 1.

```bash
[antoine@node1 ~]$ sudo chmod 550 /srv/site1
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier site 1 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du dossier site 1.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site1
```

La commande chmod qui permet de donner les autorisations de lecture à l'utilisateur et au groupe sur le fichier index.html du dossier site 1.

```bash
[antoine@node1 ~]$ sudo chmod 440 /srv/site1/index.html
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du fichier index.html du dossier site 1 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du fichier index.html du dossier site 1.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site1/index.html
```

La commande chmod qui permet de donner les autorisations de lecture à l'utilisateur et au groupe sur le dossier lost+found du dossier site 1.

```bash
[antoine@node1 ~]$ sudo chmod 440 /srv/site1/lost+found
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier lost+found du dossier site 1 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du dossier lost+found du dossier site 1.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site1/lost+found
```

La commande pour écrire dans le fichier index.html du site 2.

```bash
[antoine@node1 ~]$ sudo vi /srv/site2/index.html
```

La commande pour afficher le contenu du fichier index.html du site 2.

```bash
[antoine@node1 ~]$ sudo cat /srv/site2/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
  <h1>Coucou vous etez sur le site 2</h1>
</body>
```

La commande chmod qui permet de donner les autorisations de lecture et d'éxécution à l'utilisateur et au groupe sur le dossier site 2.

```bash
[antoine@node1 ~]$ sudo chmod 550 /srv/site2
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier site 2 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du dossier site 2.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site2
```

La commande chmod qui permet de donner les autorisations de lecture à l'utilisateur et au groupe sur le fichier index.html du dossier site 2.

```bash
[antoine@node1 ~]$ sudo chmod 440 /srv/site2/index.html
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du fichier index.html du dossier site 2 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du fichier index.html du dossier site2.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site2/index.html
```

La commande chmod qui permet de donner les autorisations de lecture à l'utilisateur et au groupe sur le dossier lost+found du dossier site 2.

```bash
[antoine@node1 ~]$ sudo chmod 440 /srv/site2/lost+found
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du dossier lost+found du dossier site 2 pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du dossier lost+found du dossier site 2.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site2/lost+found
```

Maintenant on va configurer nginx pour que les sites deux sites web puissent fonctionner et utilisent le http et gerent les erreurs.

La commande pour écrire la configuration de nginx dans le fichier de configuration de gnix est sudo vi /etc/nginx/nginx.conf

```bash
[antoine@node1 ~]$ sudo vi /etc/nginx/nginx.conf
```

La commande qui permet d'afficher toute la configuration que l'on a mise en place pour les deux sites, la gestion d'erreur et que le serveur web soit utilise le http.

```bash
[antoine@node1 ~]$ cat /etc/nginx/nginx.conf
worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;
events {
    worker_connections 1024;
}

http {
    server {
        listen 80;
        server_name node1;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1/;
        }

        location /site2 {
            alias /srv/site2/;
        }
    }
}
```

Maintenant on redemarre le service nginx pour qu'il prend la modification de la configuration en compte.

```bash
[antoine@node1 ~]$ sudo systemctl restart nginx
```

On ouvre le port 80/tcp sur le firewall pour que les sites marchent car c'est le serveur utilise le http.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

On redémarre le firewall pour qu'il prend en compte la modification de l'ouverture du port 80/tcp.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --reload
success
```

On va générer une clé et un certificat pour pouvoir mettre le seuveur web en https pour ce faire on va utiliser la commande sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout site.key

```bash
[antoine@node1 srv]$ sudo openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout site.key
 -out site.crt
Generating a 2048 bit RSA private key
.....................................+++
..................................................+++
writing new private key to 'site.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Test
Organizational Unit Name (eg, section) []:Test
Common Name (eg, your name or your server's hostname) []:node1
Email Address []:
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire de la clé pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire de la clé.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site.key
```

La commande chown qui permet de changer l'utilisateur propriétaire et le groupe propriétaire du certificat pour que ce soit l'utilisateur web et le groupe web qui soient propriétaire du certificat.

```bash
[antoine@node1 ~]$ sudo chown web:web /srv/site.crt
```

La commande pour améliorer la configuration de nginx dans le fichier de configuration de gnix est sudo vi /etc/nginx/nginx.conf

```bash
[antoine@node1 ~]$ sudo vi /etc/nginx/nginx.conf
```

La commande qui permet d'afficher les modification de la configuration que le serveur web soit utilise le https.

```bash
[antoine@node1 ~]$ cat /etc/nginx/nginx.conf
worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;
events {
    worker_connections 1024;
}

http {
    server {
        listen 80;
        server_name node1;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1/;
        }

        location /site2 {
            alias /srv/site2/;
        }
    }
    server {
        listen 443 ssl;
        server_name node1;
        ssl_certificate /srv/site.crt;
        ssl_certificate_key /srv/site.key;

        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1/;
        }

        location /site2 {
            alias /srv/site2/;
        }
    }
}
```

Maintenant on redemarre le service nginx pour qu'il prend la modification de la configuration en compte.

```bash
[antoine@node1 ~]$ sudo systemctl restart nginx
```

On ouvre le port 443/tcp sur le firewall pour que les sites marchent car c'est le serveur utilise le http et maintenant https.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
```

On redémarre le firewall pour qu'il prend en compte la modification de l'ouverture du port 443/tcp.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --reload
success
```

Maintenant on vérifier que toutes les configurations misent en place marchent et vérifie les autorisations des dossiers et fichiers. Pour ce faire on va faire la commande ls -al pour afficher les autorisations et faire des curl pour vérifier le fonctionnement des sites.

Pour vérifier que l'on a bien mis les bonnes autorisations sur le site 1 on fait la commande sudo ls -al /srv/site1

```bash
[antoine@node1 ~]$ sudo ls -al /srv/site1
total 24
dr-xr-x---. 3 web web  4096 Sep 26 22:04 .
drwxr-xr-x. 5 web web    78 Sep 26 22:18 ..
-r--r-----. 1 web web   215 Sep 26 22:04 index.html
dr--r-----. 2 web web 16384 Sep 23 15:54 lost+found
```

On voit bien que les autorisations sur le fichier index.html du site 1, que le dossier lost+found du site 1 sont bien en lecture seule pour l'utilisateur et le groupe et que les autorisations sur le dossier site 1 sont bien en lecture et exécution pour l'utilisateur et le groupe.

Pour vérifier que l'on a bien mis les bonnes autorisations sur le site 2 on fait la commande sudo ls -al /srv/site2

```bash
[antoine@node1 ~]$ sudo ls -al /srv/site2
total 24
dr-xr-x---. 3 web web  4096 Sep 24 00:19 .
drwxr-xr-x. 5 web web    78 Sep 26 22:18 ..
-r--r-----. 1 web web   216 Sep 24 00:19 index.html
dr--r-----. 2 web web 16384 Sep 23 16:13 lost+found
```

On voit bien que les autorisations sur le fichier index.html du site 2, que le dossier lost+found du site 2 sont bien en lecture seule pour l'utilisateur et le groupe et que les autorisations sur le dossier site 2 sont bien en lecture et exécution pour l'utilisateur et le groupe.

Maintenant on fait les curl pour vérfier le fonction du site 1 et du site 2 que ça soit en http ou en https.

Le curl depuis la VM node2 permet de vérifier que le site 1 qui tourne sur la VM node1 fonctionne bien en http.

```bash
[antoine@node2 ~]$ curl -kL http://node1/site1
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>
```

Le résultat du curl depuis la VM node2 nous montre que le site 1 qui tourne sur la VM node1 marche bien en http car on a bien reçu le résultat du curl du site 1.

Le curl depuis la VM node2 permet de vérifier que le site 1 qui tourne sur la VM node1 fonctionne bien en https.

```bash
[antoine@node2 ~]$ curl -kL https://node1/site1
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>
```

Le résultat du curl depuis la VM node2 nous montre que le site 1 qui tourne sur la VM node1 marche bien en https car on a bien reçu le résultat du curl du site 1.

Le curl depuis la VM node2 permet de vérifier que le site 2 qui tourne sur la VM node1 fonctionne bien en http.

```bash
[antoine@node2 ~]$ curl -kL http://node1/site2
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
  <h1>Coucou vous etez sur le site 2</h1>
</body>
```

Le résultat du curl depuis la VM node2 nous montre que le site 2 qui tourne sur la VM node1 marche bien en http car on a bien reçu le résultat du curl du site 2.

Le curl depuis la VM node2 permet de vérifier que le site 2 qui tourne sur la VM node1 fonctionne bien en https.

```bash
[antoine@node2 ~]$ curl -kL https://node1/site2
OCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
  <h1>Coucou vous etez sur le site 2</h1>
</body>
```

Le résultat du curl depuis la VM node2 nous montre que le site 2 qui tourne sur la VM node1 marche bien en https car on a bien reçu le résultat du curl du site 2.

### II. Script de sauvegarde

On va créer un utilisateur dédier pour le script de backup cet utilisateur sera l'utilisateur backup pour ce faire on va faire la commande sudo useradd -m backup.

```bash
[antoine@node1 ~]$ sudo useradd -m backup
```

```bash
[antoine@node1 ~]$ sudo usermod -G web backup
```

On va bloquer la connexion de backup pour ce faire on va dans le fichier /etc/passwd et modifier la ligne de l'utilisateur backup en remplacant /bin/bash par /bin/false pour acceder au fichier /etc/passwd on fait la commande sudo vi /etc/passwd.

```bash
[antoine@node1 ~]$ sudo vi /etc/passwd
```

La commande pour afficher le contenu du fichier /etc/passwd pour que l'on voit bien la modification que l'on a fait sur l'utilisateur backup car ça ligne était noramlement en /bin/bash mais maintenant elle est en /bin/false pour bloquer la connection.

```bash
[antoine@node1 ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:999:998:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
antoine:x:1000:1000:Antoine:/home/antoine:/bin/bash
admin:x:1001:1001::/home/admin:/bin/bash
nginx:x:998:996:Nginx web server:/var/lib/nginx:/sbin/nologin
web:x:1002:1002::/home/web:/bin/bash
backup:x:1003:1003::/home/backup:/bin/false
```

Maintenant on va créer le script de backup qui va nous permettre d'archivé et de compresser le dossier qui sera passé en argument du script, d'en garder que 7 par dossier et de mettre l'archive et la comprésion au format le nom du site_la date_l'heure pour ce faire on va faire la commande sudo vi /home/backup/tp1_backup.sh

```bash
[antoine@node1 ~]$ sudo vi /home/backup/tp1_backup.sh
```

La commande pour afficher le contenu du script de backup.

```bash
[antoine@node1 ~]$ sudo cat /home/backup/tp1_backup.sh
#!/bin/bash
# Antoine Crenn
# 26/09/2020
# Script Sauvegarde
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
chemin_site="${1}";
nom_site=${chemin_site##*/};
nom_sauvegarde="${nom_site}_${jour}_${heure}";
chemin_fermer="/home/backup/lock_${nom_site}";
chemin_sauvegarde="/home/backup/";

if test -z ${chemin_site};
then
        echo "Vous avez oublié de rentrer l'argument." >&2
        exit 1;
fi

if test ! -d ${chemin_sauvegarde};
then
        echo "Le chemin de sauvegarde ${chemin_sauvegarde} n'existe pas." >&2
        exit 1;
fi

if test ! -d ${chemin_site};
then
        echo "Le chemin du site ${chemin_site} n'existe pas." >&2;
        exit 1;
fi

if test -f ${chemin_fermer};
then
        exit 22;
fi

touch ${chemin_fermer};

tar Pzcvf ${chemin_sauvegarde}${nom_sauvegarde} ${chemin_site} --ignore-failed-read > /dev/null;

if test $? -eq 0;
then
        echo "La sauvegarde ${chemin_sauvegarde}${nom_sauvegarde} c'est bien passé.";
else
        echo "La sauvegarde ${chemin_sauvegarde}${nom_sauvegarde} a échoué." >&2;
        exit 1;
fi

nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});

if test ${nombre_sauvegarde} -ge 9;
then
        supprimer=$(ls ${chemin_sauvegarde} | grep ${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf ${chemin_sauvegarde}${supprimer};
fi

rm -rf ${chemin_fermer};
```

Les lignes heure=$(date +%H%M); et jour=$(date +%Y%m%d); permet d'obtenir la date et l'heure lors du lancement du script pour pouvoir l'utiliser pendant le script.

La lignes chemin_site="${1}"; permet de récuperer l'argument passé lors du lancement du script et la ligne nom_site=${chemin_site##*/}; permet de récuperer le nom du site sur lequel va s'éxécuté le script pour pouvoir l'utilisé plus tard.

La ligne nom_sauvegarde="${nom_site}_${jour}_${heure}"; permet de fusionnner le nom du site, la date et l'heure pour pourvoir plus tard l'utilisé pour renomé l'archive.

La ligne chemin_fermer="/home/backup/lock_${nom_site}"; permet de créer un fichier qui va bloquer l'exécution du script s'il y a en à une encore pour le site et la ligne chemin_sauvegarde="/home/backup/"; qui permet de spécifier le nom du chemin où l'on sauvegarder.

```bash
if test -z ${chemin_site};
then
        echo "Vous avez oublié de rentrer l'argument." >&2
        exit 1;
fi
```

Cette partie du code vérifie s'il y a un argument qui est tapé à la suite du script et si ce n'est pas le cas alors il renvoye une erreur correcte et arrête le script.

```bash
if test ! -d ${chemin_sauvegarde};
then
        echo "Le chemin de sauvegarde ${chemin_sauvegarde} n'existe pas." >&2
        exit 1;
fi
```

Cette partie du code vérifie s'il le dossier de sauvegarde existe et si ce n'est pas le cas alors il renvoye une erreur correcte et arrête le script.

```bash
if test ! -d ${chemin_site};
then
        echo "Le chemin du site ${chemin_site} n'existe pas." >&2;
        exit 1;
fi
```

Cette partie du code vérifie s'il le chemin du site existe et si ce n'est pas le cas alors il renvoye une erreur correcte et arrête le script.

```bash
if test -f ${chemin_fermer};
then
        exit 22;
fi

touch ${chemin_fermer};
```

Cette partie du code vérifie s'il existe le fichier lock au nom du site et si c'est pas le cas alors il arrête le script car ça veut dire qu'il y a déjà une sauvegarde en cours.

```bash
tar Pzcvf ${chemin_sauvegarde}${nom_sauvegarde} ${chemin_site} --ignore-failed-read > /dev/null;

if test $? -eq 0;
then
        echo "La sauvegarde ${chemin_sauvegarde}${nom_sauvegarde} c'est bien passé.";
else
        echo "La sauvegarde ${chemin_sauvegarde}${nom_sauvegarde} a échoué." >&2;
        exit 1;
fi
```

Cette partie du code lance l'archivage du site et la compréssion de l'archive et vérifie si l'archivage et la comprésion se sont bien passés et si ce n'est pas le cas alors il renvoye une erreur correcte et arrête le script.

```bash
nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});

if test ${nombre_sauvegarde} -ge 9;
then
        supprimer=$(ls ${chemin_sauvegarde} | grep ${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf ${chemin_sauvegarde}${supprimer};
fi

rm -rf ${chemin_fermer};
```

Cette partie du code compte le nombre d'archive du site présente dans le dossier de sauvegarde et si le nombre est supérieur ou égal à 9 alors on prend les archives du sites  on les trié puis on prend la première, on l'ajoute à une variable puis on la supprime grâce à la variable plus le chemin et après on supprime le fichier lock du site pour que l'on puisse refaire une sauvegarde plus tard.

On va utiliser la crontab pour que le script s'exécute automtique toutes les heures pour ce faire on va faire la commande sudo vi /etc/crontab.

```bash
[antoine@node1 ~]$ sudo vi /etc/crontab
```

La commande pour afficher le contenu du fichier /etc/crontab.

```bash
[antoine@node1 ~]$ cat /etc/crontab
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed

  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site1

  0  *  *  *  * backup sh /home/backup/tp1_backup.sh /srv/site2
```

On voit que le script ce lance a 0 minutes de toutes les heures et qu'il y a deux lignes pour enregistrer les deux sites.

Maintenant on cherche à restaurer une archive pour ce faire on va modifier le site puis on va faire la commande sudo tar -zxvf /home/backup/site1_20200928_1100 -C / pour restaurer une ancinne sauvegarde du site 1 pour revenir à la version sans la modification.

Le contenu du fichier index.html du site 1 avant la mdofication.

```bash
antoine@node1 ~]$ sudo cat /srv/site1/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>
```

Pour modifier le contenu du fichier index.html du site 1 on fait la commande sudo vi /srv/site1/index.html pour pouvoir écrire dans le fichier.

```bash
[antoine@node1 ~]$ sudo vi /srv/site1/index.html
```

Le contenu du fichier index.html du site 1 après la modifcation qui est l'ajout de la ligne `<p>Test</p>`.

```bash
[antoine@node1 ~]$ sudo cat /srv/site1/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
<p>Test</p>
</body>
```

Maintenant on veut faire la restauration de l'ancinne version du site pour ce faire on va faire la commande sudo tar -zxvf /home/backup/site1_20200928_1100 -C / qui nous permet de decompresser et d'extraire depuis l'archive et le remplacer dans le dossier du site 1.

```bash
[antoine@node1 ~]$ sudo tar -zxvf /home/backup/site1_20200928_1100 -C /
srv/site1/
srv/site1/lost+found/
srv/site1/index.html
```

Le contenu du fichier index.html du site 1 après la restauration grâce à une archive. On voit que la modification `<p>Test</p>` n'est plus présente dans le contenu du fichier index.html du site 1.

```bash
[antoine@node1 ~]$ sudo cat /srv/site1/index.html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Page Accueil</title>
  <meta name="description" content="Page Accueil" />
</head>

<body>
 <h1> Coucou vous etez sur le site 1</h1>
</body>
```

### III. Surveillance, alerte

Dans cette partie, on veut installer l'outil Netdata pour surveiller et pour que l'on recoit les alertes dans un salon Discord dédié.

Pour installer l'outil Netadata on fait la commande suivante.

```bash
[antoine@node1 ~]$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

Mais il faut aussi ouvrir le port associé à Netdata pour que l'on puissent acceder au tableau de bord Netdata ce port est le 19999 on l'ouvre avec la commande suivante.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[sudo] password for antoine:
success
```

Et on redemarre le firewall pour que l'ajout du port soit pris en compte.

```bash
[antoine@node1 ~]$ sudo firewall-cmd --reload
success
```

Pour vérifier le bon fonctionnement de Netdata il faut tapper l'adresse IP de la machine où Netdata est lancé dans un navigateur puis il faut mettre le port utilisé par Netdata pour notre cas actuel il faut tapper 192.168.1.11:19999 car le Netdata est lancé sur la VM node1.

Le tabeau de bord Netdata pour la VM node1.

![](https://i.imgur.com/PDUAy11.png)

On veut configurer Netdata pour qu'il nous envoye des alertes dans un salon Discord dédié. Il faut créer un salon Discord dans un serveur puis cliquer sur l'engrenage à droite du salon, aller dans intégrations, cliquer sur Créer un webhook, le renomé si envie, copié l'url du webhook puis il faut aller dans le fichier /etc/netdata/edit-config health_alarm_notify.conf avec la commande sudo /etc/netdata/edit-config health_alarm_notify.conf trouvé la ligne DISCORD_WEBHOOK_URL="" et placer entre les doubles côtes l'url du webhook et on remplace la ligne DEFAULT_RECIPIENT_DISCORD="" par la ligne DEFAULT_RECIPIENT_DISCORD="alarms".

Un exemple d'alarme envoyé par Netdata via un salon discord.

![](https://i.imgur.com/8xeIbXy.png)

